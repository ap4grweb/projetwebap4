const {
  DataTypes,
} = require('sequelize');

module.exports = (sequelize) => {
  const attributes = {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'ID',
      unique: 'ID',
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'name',
    },
    img: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'img',
    },
    author: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'author',
      references: {
        key: 'ID',
        model: 'User_model',
      },
    },
    creationTime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('curtime'),
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'creationTime',
    },
  };
  const options = {
    tableName: 'Template',
    comment: '',
    indexes: [{
      name: 'author',
      unique: false,
      type: 'BTREE',
      fields: ['author'],
    }],
    timestamps: false,
  };
  const TemplateModel = sequelize.define('Template_model', attributes, options);
  return TemplateModel;
};
