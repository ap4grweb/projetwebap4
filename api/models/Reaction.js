const {
  DataTypes,
} = require('sequelize');

module.exports = (sequelize) => {
  const attributes = {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'ID',
      unique: 'ID',
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'name',
    },
    value: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'value',
    },
    isSuper: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'isSuper',
    },
  };
  const options = {
    tableName: 'Reaction',
    comment: '',
    indexes: [],
    timestamps: false,
  };
  const ReactionModel = sequelize.define('Reaction_model', attributes, options);
  return ReactionModel;
};
