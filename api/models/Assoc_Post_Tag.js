const {
  DataTypes,
} = require('sequelize');

module.exports = (sequelize) => {
  const attributes = {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'ID',
      unique: 'ID',
    },
    postRef: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'postRef',
      references: {
        key: 'ID',
        model: 'Post_model',
      },
    },
    tagRef: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'tagRef',
      references: {
        key: 'ID',
        model: 'Tag_model',
      },
    },
  };
  const options = {
    tableName: 'Assoc_Post_Tag',
    comment: '',
    indexes: [{
      name: 'postRef',
      unique: false,
      type: 'BTREE',
      fields: ['postRef'],
    }, {
      name: 'tagRef',
      unique: false,
      type: 'BTREE',
      fields: ['tagRef'],
    }],
    timestamps: false,
  };
  const AssocPostTagModel = sequelize.define('Assoc_Post_Tag_model', attributes, options);
  return AssocPostTagModel;
};
