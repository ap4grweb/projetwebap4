const bcrypt = require('bcrypt');
const {
  DataTypes,
} = require('sequelize');

module.exports = (sequelize) => {
  const attributes = {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'ID',
      unique: 'ID',
    },
    username: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'username',
    },
    mail: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'mail',
    },
    password: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'password',
    },
    bio: {
      type: DataTypes.STRING(140),
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'bio',
    },
    img: {
      type: DataTypes.STRING(50),
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'img',
    },
    theme: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'theme',
      references: {
        key: 'ID',
        model: 'Theme_model',
      },
    },
    registerTime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('curtime'),
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'registerTime',
    },
    lastLoginTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'lastLoginTime',
    },
    lastFailedLogin: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'lastFailedLogin',
    },
    loginAttemptCnt: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0',
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'loginAttemptCnt',
    },
    superReactionsAvailable: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0',
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'superReactionsAvailable',
    },
  };
  const options = {
    tableName: 'User',
    comment: '',
    indexes: [{
      name: 'theme',
      unique: false,
      type: 'BTREE',
      fields: ['theme'],
    }],
    hooks: {
      // Hash password before save
      beforeCreate: (user) => {
        const salt = bcrypt.genSaltSync();
        user.password = bcrypt.hashSync(user.password, salt);
      },
    },
    timestamps: false,
  };
  const UserModel = sequelize.define('User_model', attributes, options);

  // Creating a method to check password hash
  UserModel.prototype.validPassword = function (password) {
    return bcrypt.compare(password, this.password);
  };
  return UserModel;
};
