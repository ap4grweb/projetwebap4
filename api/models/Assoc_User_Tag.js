const {
  DataTypes,
} = require('sequelize');

module.exports = (sequelize) => {
  const attributes = {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'ID',
      unique: 'ID',
    },
    userRef: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'userRef',
      references: {
        key: 'ID',
        model: 'User_model',
      },
    },
    tagRef: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'tagRef',
      references: {
        key: 'ID',
        model: 'Tag_model',
      },
    },
  };
  const options = {
    tableName: 'Assoc_User_Tag',
    comment: '',
    indexes: [{
      name: 'userRef',
      unique: false,
      type: 'BTREE',
      fields: ['userRef'],
    }, {
      name: 'tagRef',
      unique: false,
      type: 'BTREE',
      fields: ['tagRef'],
    }],
    timestamps: false,
  };
  const AssocUserTagModel = sequelize.define('Assoc_User_Tag_model', attributes, options);
  return AssocUserTagModel;
};
