const {
  DataTypes,
} = require('sequelize');

module.exports = (sequelize) => {
  const attributes = {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'ID',
      unique: 'ID',
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'name',
    },
    description: {
      type: DataTypes.STRING(140),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'description',
    },
    img: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'img',
    },
    threshold: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'threshold',
    },
  };
  const options = {
    tableName: 'Ranking',
    comment: '',
    indexes: [],
    timestamps: false,
  };
  const RankingModel = sequelize.define('Ranking_model', attributes, options);
  return RankingModel;
};
