const {
  DataTypes,
} = require('sequelize');

module.exports = (sequelize) => {
  const attributes = {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'ID',
      unique: 'ID',
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'name',
    },
  };
  const options = {
    tableName: 'Tag',
    comment: '',
    indexes: [],
    timestamps: false,
  };
  const TagModel = sequelize.define('Tag_model', attributes, options);
  return TagModel;
};
