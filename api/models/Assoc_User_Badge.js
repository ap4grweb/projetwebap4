const {
  DataTypes,
} = require('sequelize');

module.exports = (sequelize) => {
  const attributes = {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'ID',
      unique: 'ID',
    },
    userRef: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'userRef',
      references: {
        key: 'ID',
        model: 'User_model',
      },
    },
    badgeRef: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'badgeRef',
      references: {
        key: 'ID',
        model: 'Badge_model',
      },
    },
    obtained: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('curtime'),
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'obtained',
    },
  };
  const options = {
    tableName: 'Assoc_User_Badge',
    comment: '',
    indexes: [{
      name: 'userRef',
      unique: false,
      type: 'BTREE',
      fields: ['userRef'],
    }, {
      name: 'badgeRef',
      unique: false,
      type: 'BTREE',
      fields: ['badgeRef'],
    }],
    timestamps: false,
  };
  const AssocUserBadgeModel = sequelize.define('Assoc_User_Badge_model', attributes, options);
  return AssocUserBadgeModel;
};
