const {
  DataTypes,
} = require('sequelize');

module.exports = (sequelize) => {
  const attributes = {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'ID',
      unique: 'ID',
    },
    userRef: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'userRef',
      references: {
        key: 'ID',
        model: 'User_model',
      },
    },
    reactionRef: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'reactionRef',
      references: {
        key: 'ID',
        model: 'Reaction_model',
      },
    },
    postRef: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'postRef',
      references: {
        key: 'ID',
        model: 'Post_model',
      },
    },
    reactionTime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('curtime'),
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'reactionTime',
    },
  };
  const options = {
    tableName: 'Assoc_User_Reaction_Post',
    comment: '',
    indexes: [{
      name: 'userRef',
      unique: false,
      type: 'BTREE',
      fields: ['userRef'],
    }, {
      name: 'reactionRef',
      unique: false,
      type: 'BTREE',
      fields: ['reactionRef'],
    }, {
      name: 'postRef',
      unique: false,
      type: 'BTREE',
      fields: ['postRef'],
    }],
    timestamps: false,
  };
  const AssocUserReactionPostModel = sequelize.define('Assoc_User_Reaction_Post_model', attributes, options);
  return AssocUserReactionPostModel;
};
