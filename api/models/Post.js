const {
  DataTypes,
} = require('sequelize');

module.exports = (sequelize) => {
  const attributes = {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'ID',
      unique: 'ID',
    },
    template: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'template',
      references: {
        key: 'ID',
        model: 'Template_model',
      },
    },
    author: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'author',
      references: {
        key: 'ID',
        model: 'User_model',
      },
    },
    caption: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'caption',
    },
    public: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'public',
    },
    creationTime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('curtime'),
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'creationTime',
    },
  };
  const options = {
    tableName: 'Post',
    comment: '',
    indexes: [{
      name: 'template',
      unique: false,
      type: 'BTREE',
      fields: ['template'],
    }],
    timestamps: false,
  };
  const PostModel = sequelize.define('Post_model', attributes, options);
  return PostModel;
};
