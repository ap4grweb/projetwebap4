const router = require('express').Router();
const postsController = require('../controllers/posts.controller');
const feedController = require('../controllers/feed.controller');
const { requireAuth } = require('../middleware/auth.middleware');
const { checkPublicPost } = require('../middleware/publicPost.middleware');

/**
 * @post / - Create a new post
 */
router.post('/', requireAuth, postsController.createPost);

/**
 * @get /tag/:tag - Get a random post by tag
 * @param {string} tag - Tag name
 */
router.get('/tag/:tag', postsController.getByTag);

/**
 * @get /history - Get the posts with which the user
 *                 has interacted
 */
router.get('/history', postsController.getHistory);

/**
 * @get /mine - Get the list of the user posts
 */
router.get('/mine', postsController.getMyPosts);

/**
 * /feed - Router for the feed of the user (tags subscribed)
 * @get / - Get a random post from the user feed
 * @post / - Add a new tag to the user tag list
 * @delete / - Remove a tag of the user tag list
 */
router.route('/feed')
  .get(requireAuth, feedController.getFeed)
  .post(requireAuth, feedController.postTag)
  .delete(requireAuth, feedController.removeTag);

/**
 * @get /feed/mytags - Get list of the user tags
 *  */ 
router.get('/feed/mytags', requireAuth, feedController.myTags);

/**
 * /:id - Router to work with a specific post
 * @param {number} id - Unique id of the post
 * @get / - Get the post
 * @patch / - Put a reaction on the post
 * @remove / - Delete post if the user own it
 */
router.route('/:id')
  .get(requireAuth, postsController.getSinglePost)
  .patch(requireAuth, postsController.putReaction)
  .delete(requireAuth, postsController.removePost);

/**
 * @get /public/:id - Fetch a post that is public (don't require account)
 * @param {number} id - Unique id of the post
 */
router.get('/public/:id', checkPublicPost, postsController.getSinglePost);

/**
 * @get /:id/score - Fetch a score of a post
 * @param {number} id - Unique id of the post
 */
router.get('/:id/score', requireAuth, postsController.getPostScore);

/**
 * @get /public/:id/score - Fetch a score of a public post (don't require account)
 * @param {number} id - Unique id of the post
 */
router.get('/public/:id/score', checkPublicPost, postsController.getPostScore);

/**
* @get /:id/author - Fetch a score of a post
* @param {number} id - Unique id of the post
*/
router.get('/:id/author', requireAuth, postsController.getPostAuthor);

/**
* @get /public/:id/author - Fetch a score of a public post (don't require account)
* @param {number} id - Unique id of the post
*/
router.get('/public/:id/author', checkPublicPost, postsController.getPostAuthor);

module.exports = router;
