const router = require('express').Router();

// Middleware
const { uploadTemplate } = require('../middleware/upload.middleware');
const { requireAuth } = require('../middleware/auth.middleware');

// Controllers
const templatesController = require('../controllers/templates.controller');

/**
 * @get / - Get the list of the templates
 */
router.get('/', requireAuth, templatesController.getAllTemplates);

/**
 * @post / - Upload a new template to the server
 */
router.post('/', requireAuth, uploadTemplate.single('file'), templatesController.postTemplate);

/**
 * @get /:id - Fetch a single template
 * @param {number} id - Unique ID of the template
 */
router.get('/:id', templatesController.getSingleTemplate);

/**
 * @delete /:id - Remove a template if the user is the owner
 * @param {number} id - Unique ID of the template
 */
router.delete('/:id', requireAuth, templatesController.removeTemplate);

module.exports = router;
