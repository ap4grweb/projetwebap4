const express = require('express');

const router = express.Router();
const { requireAuth } = require('../middleware/auth.middleware');

// Making 1 route per folder to fetch all images of the server

// User
router.use('/profile', express.static('uploads/profile'));
router.use('/templates', express.static('uploads/templates'));

// Don't allow not connected users to fetch badges images
router.use('/badges', requireAuth, express.static('uploads/badges'));

module.exports = router;
