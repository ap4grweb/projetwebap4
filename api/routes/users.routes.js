const router = require('express').Router();

// Middleware
const { requireAuth } = require('../middleware/auth.middleware');
const { uploadProfile } = require('../middleware/upload.middleware');

// Controllers
const authController = require('../controllers/auth.controller');
const userController = require('../controllers/user.controller');

/**
 * @post /register - Register a new account
 */
router.post('/register', authController.signUp);

/**
 * @post /login - Login with an account
 */
router.post('/login', authController.signIn);

/**
 * @get /logout - Logout of the platform
 */
router.get('/logout', requireAuth, authController.logout);

/**
 * @get /:id/badges - Fetch all badges of a user
 * @param {number} id - Unique ID of the user
 */
router.get('/:id/badges', requireAuth, userController.getUserBadges);

/**
 * Router /:id to work with an user
 * @param {number} id - Unique ID of the user
 * @get Fetch a user
 * @delete Remove account if the user is the owner of it
 * @patch Editing profile (bio, profilePic, ...)
 */
router.get('/:id', requireAuth, userController.getUser);
router.delete('/:id', requireAuth, userController.deleteUser);
router.patch('/:id', requireAuth, uploadProfile.single('file'), userController.editProfile);

/**
 * @get /photo/mail=:mail - Fetch a user profile pic by mail to
 *                          display it at login
 * @param {string} mail - Mail of the user
 */
router.get('/photo/mail=:mail', userController.getPhoto);

/**
 * @get /photo/username=:username - Fetch a user profile pic by username to
 *                                  display it at login
 * @param {string} username - Username of the user
 */
router.get('/photo/username=:username', userController.getPhoto);

module.exports = router;
