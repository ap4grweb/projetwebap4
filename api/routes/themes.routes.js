const router = require('express').Router();
const { requireAuth } = require('../middleware/auth.middleware');
const themesController = require('../controllers/themes.controller');

/**
 * @get / - Fetch all the themes available on the platform
 */
router.get('/', themesController.getAllThemes);

/**
 * @get /:id - Fetch a single theme
 * @param {string} id - Unique ID of the theme
 */
router.get('/:id', themesController.getSingleTheme);

module.exports = router;
