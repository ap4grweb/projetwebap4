const router = require('express').Router();
const homeController = require('../controllers/home.controller');

/**
 * @get / - The home route of the api
 */
router.get('/', homeController.getHome);

module.exports = router;
