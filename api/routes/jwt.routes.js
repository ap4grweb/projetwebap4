const router = require('express').Router();
const { requireAuth } = require('../middleware/auth.middleware');

/**
 * @get / - Return the ID of the user if he is authenticated
 */
router.get('/', requireAuth, (req, res) => {
  res.send({ id: res.locals.user.ID });
});

module.exports = router;
