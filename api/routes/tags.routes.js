const router = require('express').Router();
const { requireAuth } = require('../middleware/auth.middleware');
const tagsController = require('../controllers/tags.controller');

/**
 * @get / - Get list of the tags available on the platform
 */
router.get('/', requireAuth, tagsController.getAllTags);

module.exports = router;
