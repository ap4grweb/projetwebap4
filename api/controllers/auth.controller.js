const jwt = require('jsonwebtoken');
const { User } = require('../config/db');

/**
 * Creation of token valid for 3 days
 */
const maxAge = 3 * 24 * 60 * 60 * 1000;
const createToken = (id) => jwt.sign({ id }, process.env.TOKEN_SECRET, {
  expiresIn: maxAge,
});

/**
 * Controller to sign up new user
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.signUp = async (req, res) => {
  const { username, mail, password } = req.body;
  try {
    const user = User.build({
      username, mail, password,
    });
    await user.save();
    // user found and password ok
    const token = createToken(user.ID);
    res.cookie('jwt', token, { httpOnly: true, maxAge });
    res.status(201).json({ user: user.ID });
  } catch (err) {
    res.status(200).send(err.toString());
  }
};

/**
 * Controller to sign in user
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.signIn = async (req, res) => {
  const { username, mail, password } = req.body;
  let userQuery = {};
  if (username) userQuery = { username };
  else if (mail) userQuery = { mail };
  try {
    User.findOne({ where: userQuery })
      .then(async (user) => {
        if (!user) {
          // user not found in db
          res.send('User not found !');
        } else if (!await user.validPassword(password)) {
          // user found but invalid password
          res.send('Invalid password !');
        } else {
          // user found and password ok
          const token = createToken(user.ID);
          res.cookie('jwt', token, { httpOnly: true, maxAge });
          res.status(200).json({ user: user.ID });
        }
      });
  } catch (err) {
    console.log(err);
    res.status(200).json(err.toString());
  }
};

module.exports.logout = (req, res) => {
  res.cookie('jwt', '', { maxAge: 1 });
  res.redirect('/');
};
