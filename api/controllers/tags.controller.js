const { Tag } = require('../config/db');

/**
 * Get list of tags available
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getAllTags = async (req, res) => {
  Tag.findAll()
    .then((tags) => res.send(tags))
    .catch(() => res.sendStatus(404));
};
