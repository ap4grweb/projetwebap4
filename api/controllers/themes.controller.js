const { Theme } = require('../config/db');

/**
 * Fetch all themes of the platform
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getAllThemes = async (req, res) => {
  Theme.findAll()
    .then((themes) => res.send(themes))
    .catch(() => res.sendStatus(404));
};

/**
 * Fetch a single theme
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getSingleTheme = async (req, res) => {
  const { id } = req.params;
  const theme = await Theme.findOne({ where: { ID: id } });
  if (theme) {
    res.send(theme);
  } else {
    res.sendStatus(404);
  }
};
