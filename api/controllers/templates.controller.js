const { Op } = require('sequelize');

// Db model
const { Template } = require('../config/db');

// Utils
const { deleteImg, deleteExifs } = require('../utils/utils');

/**
 * Convert a template image name to the url
 * @param {Template} template - Template model (database)
 * @returns {string} - URL to the template image
 */
const convertTemplate = (template) => {
  template.img = `/images/templates/${template.img}`;
  return template;
};

/**
 * Get list of all the templates
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getAllTemplates = (req, res) => {
  let { limit, keyword, offset } = req.query;

  // If no keyword given --> empty string
  keyword = keyword || '';

  // Limit query to 10 maximum
  limit = (!limit || limit > 20 || limit <= 0) ? 20 : Number(limit);

  // Check if page number is positive (default is 0)
  offset = (!offset || offset < 0) ? 0 : Number(offset);

  // Make a filter based on offset and limit
  const filter = { offset, limit };

  // Add keyword filter if it isn't empty
  if (keyword !== '') filter.where = { name: { [Op.like]: `%${keyword}%` } };

  // Querying the db
  Template.findAll(filter)
    .then((templates) => templates.map(convertTemplate))
    .then((templates) => res.send(templates))
    .catch(() => res.sendStatus(404));
};

/**
 * Get a single template
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getSingleTemplate = async (req, res) => {
  const { id } = req.params;
  const template = await Template.findOne({ where: { ID: id } });
  if (template) {
    res.send(convertTemplate(template));
  } else {
    res.sendStatus(404);
  }
};

/**
 * Post a new template
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.postTemplate = async (req, res) => {
  const { name } = req.body;
  const { filename: img, destination } = req.file;
  const author = res.locals.user.ID;

  deleteExifs(`${destination}/${img}`);

  if (name && img) {
    const newTemplate = Template.build({ name, img, author });
    await newTemplate.save()
      .then((template) => res.send({ id: template.ID }))
      .catch(() => res.sendStatus(404));
  } else {
    res.sendStatus(404);
  }
};

/**
 * Remove a template
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.removeTemplate = async (req, res) => {
  const { id } = req.params;
  const query = { id, author: res.locals.user.ID.toString() };
  const template = await Template.findOne({ where: query });
  if (template) {
    Template.destroy({ where: query })
      .then((result) => {
        if (result) {
          deleteImg(`templates/${template.img}`);
          res.sendStatus(204);
        } else {
          res.sendStatus(401);
        }
      })
      .catch((err) => {
        // I'm a teapot
        res.sendStatus(418);
      });
  } else {
    res.sendStatus(404);
  }
};
