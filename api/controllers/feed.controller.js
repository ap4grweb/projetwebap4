const { Op } = require('sequelize');

// Db models
const {
  User, Tag, Post, Assoc_User_Tag, Assoc_User_Reaction_Post,
} = require('../config/db');

/**
 * @param {number} id - Unique ID of the user
 * @returns {[number]} - List of the user tags (name)
 */
const getTags = (id) => User.findOne({
  where: { ID: id },
  include: {
    model: Tag,
    as: 'tags',
    through: {
      // Prevent to include the assoc table with the json
      attributes: [],
    },
  },
})
  .then((user) => user.tags)
  .catch(console.error);

/**
 * Get a post from the feed of the user
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getFeed = async (req, res) => {
  const id = res.locals.user.ID;

  let userTags = await getTags(id);
  userTags = userTags.map((tag) => tag.ID);

  // Get posts posts with which the user has already reacted
  const excludedPosts = await Assoc_User_Reaction_Post.findAll({
    attributes: ['postRef'],
    where: {
      userRef: id,
    },
  })
    .then((posts) => posts.map((post) => post.postRef));

  // Find a post excluded the already reacted posts
  const post = await Post.findOne({
    where: { ID: { [Op.notIn]: excludedPosts } },
    include: [
      {
        model: Tag,
        as: 'tags',
        where: { ID: { [Op.in]: [userTags] } },
        through: {
          include: [],
        },
      },
    ],
  }).catch((err) => {
    console.error(err);
    res.sendStatus(404);
  });
  res.send(post);
};

/**
 * Get list of user tags
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.myTags = (req, res) => {
  const id = res.locals.user.ID;
  getTags(id)
    .then((tags) => res.send(tags))
    .catch((err) => {
      console.error(err);
      res.send(404);
    });
};

/**
 * Get tags of a post
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.postTag = async (req, res) => {
  const userId = res.locals.user.ID;

  const { tag: tagName } = req.body;

  // Check if the tag exist
  const tagQuery = await Tag.findOne({
    attributes: ['ID'],
    where: { name: tagName.toLowerCase() },
  })
    .catch((err) => {
      console.error(err);
      res.sendStatus(404);
    });
  let tagId;

  if (tagQuery == null) {
    // Need to create the tag
    const newTag = Tag.build({ name: tagName.toLowerCase() });
    await newTag.save()
      .then((tag) => {
        tagId = tag.ID;
      })
      .catch((err) => {
        console.error(err);
        res.sendStatus(404);
      });
  } else {
    // If the tag already exist, just take it's id
    tagId = tagQuery.ID;
  }

  const userTags = await getTags(userId);
  if (userTags.findIndex(({ ID }) => ID === tagId) !== -1) {
    // If the user already have this tag
    res.sendStatus(200);
  } else {
    const newUserTag = Assoc_User_Tag.build({ userRef: userId, tagRef: tagId });
    newUserTag.save()
      .then(({ ID }) => res.send({ ID }))
      .catch((err) => {
        console.error(err);
        res.sendStatus(404);
      });
  }
};

/**
 * Remove a tag of user feed
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.removeTag = async (req, res) => {
  const userId = res.locals.user.ID;
  const { tag: tagName } = req.body;
  const userTags = await getTags(userId);
  userTags.forEach(async (tag) => {
    if (tag.name.toLowerCase() === tagName.toLowerCase()) {
      await Assoc_User_Tag.destroy({
        where: { userRef: userId, tagRef: tag.ID },
      })
        .catch(console.error);
    }
  });

  res.sendStatus(200);
};
