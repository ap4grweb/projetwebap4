/**
 * Api home route
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getHome = (req, res) => {
  res.send('Welcome to the api home !');
};
