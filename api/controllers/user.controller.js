// Db models
const { User, Badge } = require('../config/db');

// Utils
const { deleteImg } = require('../utils/utils.js');

/**
 * Check if a user is the owner of an id
 * @param {express.Response} res - Response
 * @param {number} id - Id to check
 * @returns {boolean} - True if the user is the owner of this id
 */
const isItMyAccount = (res, id) => res.locals.user.ID.toString() === id;

/**
 * Get the profile image of a user
 * @param {number} id - Unique ID of the user
 * @returns {string} - URL to image
 */
const getProfilePicPath = async (id) => {
  const user = await User.findByPk(id, { attributes: ['img'] });
  const img = user?.img;
  if (img !== undefined) {
    // user not found
    return `profile/${img}`;
  }
  return '';
};

/**
 * Fetch an user
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getUser = async (req, res) => {
  const { id } = req.params;

  if (isItMyAccount(res, id)) {
    res.send(res.locals.user.dataValues);
  } else {
    // User try to fetch user profile of someone else
    res.sendStatus(401);
  }
};

/**
 * Get list of user badges
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getUserBadges = async (req, res) => {
  const { id } = req.params;

  User.findOne({
    where: { ID: id },
    include: {
      model: Badge,
      as: 'badges',
      through: {
        // Prevent to include the assoc table with the json
        attributes: [],
      },
    },
  })
    .then((user) => user.badges)
    .then((badges) => badges.map((badge) => {
      badge.img = `images/badges/${badge.img}`;
      return badge;
    }))
    .then((badges) => res.send(badges))
    .catch(() => res.sendStatus(418));
};

/**
 * Remove a user
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.deleteUser = (req, res) => {
  const { id } = req.params;

  if (isItMyAccount(res, id)) {
    User.destroy({ where: { id } })
      .then(() => {
        // Maybe redirect to /logout in front can be better
        res.cookie('jwt', '', { maxAge: 1 });
        res.sendStatus(204);
      })
      .catch((err) => {
        // I'm a teapot
        res.sendStatus(418);
      });
  } else {
    // User try to delete user profile of someone else
    res.sendStatus(401);
  }
};

/**
 * Edit profile
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.editProfile = async (req, res) => {
  const { id } = req.params;
  const { bio } = req.body;

  const img = req?.file?.filename;

  const oldImg = await getProfilePicPath(id);

  if (isItMyAccount(res, id)) {
    User.update({ bio, img }, {
      where: {
        ID: id,
      },
    })
      .then(console.log)
      .then(() => res.sendStatus(200))
      .then(() => {
        if (img !== undefined && oldImg !== '') {
          deleteImg(oldImg);
        }
      })
      .catch((err) => {
        console.log(err);
        // I'm a teapot
        res.sendStatus(418);
      });
  } else {
    // User try to edit user profile of someone else
    res.sendStatus(401);
  }
};

/**
 * Get photo of a user
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getPhoto = async (req, res) => {
  const { username, mail } = req.params;

  // Determine if the user request photo by mail or username
  let filter = {};
  if (username) filter = { username };
  else if (mail) filter = { mail };

  const user = await User.findOne({ attributes: ['img'], where: filter });
  const img = user?.img;
  if (img === undefined) {
    // user not found
    res.sendStatus(204);
  } else if (img === null) {
    // user don't have picture
    res.send({ url: 'images/profile/default.jpg' });
  } else {
    res.send({ url: `images/profile/${user.img}` });
  }
};
