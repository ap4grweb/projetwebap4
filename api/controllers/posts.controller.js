const { Op } = require('sequelize');

// Db models
const {
  Post, User, Tag, Assoc_User_Reaction_Post, Reaction, Assoc_Post_Tag,
} = require('../config/db');

/**
 * Get the tag ID associated to a tag name
 * @async
 * @param {string} tagName - Tag name to find
 * @returns {number} - ID of the tag in database
 */
const getTagID = async (tagName) => {
  const result = await Tag.findOne({ where: { name: tagName } });
  // If the tag exist
  if (result !== null) {
    return result.dataValues.ID;
  }
  // Create tag if doesn't exist
  const newTag = Tag.build({ name: tagName });
  const { ID: newTagId } = await newTag.save();

  return newTagId;
};

/**
 * Create a new post
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.createPost = async (req, res) => {
  const userId = res.locals.user.ID;
  const {
    template, caption, publicVisibility, tags,
  } = req.body;

  const newPost = Post.build(
    {
      template,
      caption,
      author: userId,
      public: publicVisibility,
    },
  );
  const { ID: postID } = await newPost.save();

  // Getting tags ID's
  const tagsId = await Promise.all(tags.map(getTagID));

  // Adding tags to the post
  Promise.all(tagsId.map(async (tag) => {
    const newAssoc = Assoc_Post_Tag.build({ tagRef: tag, postRef: postID });
    return newAssoc.save();
  })).then(() => res.send({ ID: postID }))
    .catch((err) => {
      console.error(err);
      res.sendStatus(404);
    });
};

/**
 * Fetch a single post
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getSinglePost = (req, res) => {
  const { id } = req.params;
  Post.findByPk(id, {
    include: [
      {
        attributes: ['ID', 'username', 'img'],
        model: User,
        as: 'postAuthor',
      }],
  })
    .then((post) => post)
    .then((post) => res.send(post))
    .catch((err) => {
      console.error(err);
      res.sendStatus(404);
    });
};

/**
 * Get a random post by tag name
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getByTag = async (req, res) => {
  const id = res.locals.user.ID;
  const { tag } = req.params;

  // Get posts posts with which the user has already reacted
  const excludedPosts = await Assoc_User_Reaction_Post.findAll({
    attributes: ['postRef'],
    where: {
      userRef: id,
    },
  })
    .then((posts) => posts.map((post) => post.postRef));

  let post;
  // Random post (no matter the tag)
  if (tag !== 'random') {
    post = await Post.findOne({
      where: { ID: { [Op.notIn]: excludedPosts } },
      include: [
        {
          model: Tag,
          as: 'tags',
          where: { name: tag },
        },
      ],
    })
      .catch(console.err);
  } else {
    post = await Post.findOne({
      where: { ID: { [Op.notIn]: excludedPosts } },
      include: [
        {
          model: Tag,
          as: 'tags',
        },
      ],
    })
      .catch(console.err);
  }
  res.send(post);
};

/**
 * Add a reaction to a post
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.putReaction = async (req, res) => {
  const userId = res.locals.user.ID;
  const { reaction } = req.body;
  const { id: post } = req.params;
  // Check if the user has already reacted to this post
  const authorizedToPost = await Assoc_User_Reaction_Post.findOne({
    where: {
      userRef: userId,
      postRef: post,
    },
  })
    .then((result) => (result == null))
    .catch(console.error);

  if (authorizedToPost) {
    const { ID: reactionID } = await Reaction.findOne({
      where: { name: reaction },
    });

    const newReaction = Assoc_User_Reaction_Post.build({
      userRef: userId,
      reactionRef: reactionID,
      postRef: parseInt(post),
    });

    newReaction.save()
      .then(({ ID }) => res.send({ ID }))
      .catch((err) => {
        console.log(err);
        res.sendStatus(401);
      });
  } else {
    res.sendStatus(401);
  }
};

/**
 * Delete a post
  * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.removePost = (req, res) => {
  const { id } = req.params;
  const userId = res.locals.user.ID;

  Post.destroy({ where: { id, author: userId } })
    .then(() => {
      res.sendStatus(204);
    })
    .catch(() => {
      res.sendStatus(401);
    });
};

/**
 * Get history of the reactions of the user to others posts
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getHistory = async (req, res) => {
  const id = res.locals.user.ID;

  let { reactedPosts } = await User.findOne({
    where: { ID: id },
    attributes: [],
    include: {
      model: Post,
      as: 'reactedPosts',
      through: {
        attributes: [],
      },
    },
  })
    .catch(console.error);

  reactedPosts = reactedPosts.map((post) => post.dataValues);
  reactedPosts = await Promise.all(reactedPosts.map(async (post) => {
    const { reactionRef } = await Assoc_User_Reaction_Post.findOne(
      {
        attributes: ['reactionRef'],
        where: {
          postRef: post.ID,
          userRef: id,
        },
      },
    );
    const { name: reaction } = await Reaction.findByPk(reactionRef);
    return { ...post, reaction };
  }));
  console.log(reactedPosts);
  res.send(reactedPosts);
};

/**
 * Get the list of my uploaded posts
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getMyPosts = async (req, res) => {
  const id = res.locals.user.ID;

  const { posts } = await User.findByPk(id, {
    attributes: [],
    include: [
      {
        model: Post,
        as: 'posts',
      },
    ],
  })
    .catch(console.error);
  res.send(posts);
};

/**
 * Get score of a post
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getPostScore = async (req, res) => {
  const { id } = req.params;

  // Get all of the reactions for a post
  const reactions = await Post.findByPk(id, {
    attributes: [],
    include: [
      {
        attributes: ['value'],
        model: Reaction,
        as: 'reactions',
        through: {
          // Prevent the inclusion of the assoc table with the json
          attributes: [],
        },
      }],
  })
    .then((post) => post.reactions)
    .catch((err) => {
      console.error(err);
      res.sendStatus(404);
    });

  // Compute the sum of the reaction's values
  const score = reactions.reduce((total, reaction) => total + reaction.value, 0);
  res.send({ score });
};

/**
 * Get author of a post (name)
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 */
module.exports.getPostAuthor = async (req, res) => {
  const { id } = req.params;

  // Get all of the reactions for a post
  const { author: authorId } = await Post.findByPk(id, {
    attributes: ['author'],
  })
    .catch((err) => {
      console.error(err);
      res.sendStatus(404);
    });
  const { username } = await User.findByPk(authorId, {
  });
  res.send({ username });
};
