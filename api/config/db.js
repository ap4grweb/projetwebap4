const { Sequelize } = require('sequelize');

// Creating db instance
const sequelize = new Sequelize('captionYourMeme', 'root', 'root', {
  host: process.env.DB_HOST || 'db',
  port: 3306,
  dialect: 'mariadb',
  dialectOptions: {
    connectTimeout: 1000,
  },
});

const db = {};

db.sequelize = sequelize;

// export standard models
db.User = require('../models/User')(sequelize);
db.Theme = require('../models/Theme')(sequelize);
db.Template = require('../models/Template')(sequelize);
db.Tag = require('../models/Tag')(sequelize);
db.Reaction = require('../models/Reaction')(sequelize);
db.Ranking = require('../models/Ranking')(sequelize);
db.Post = require('../models/Post')(sequelize);
db.Badge = require('../models/Badge')(sequelize);

// export associated tables
db.Assoc_User_Tag = require('../models/Assoc_User_Tag')(sequelize);
db.Assoc_User_Reaction_Post = require('../models/Assoc_User_Reaction_Post')(sequelize);
db.Assoc_User_Badge = require('../models/Assoc_User_Badge')(sequelize);
db.Assoc_Post_Tag = require('../models/Assoc_Post_Tag')(sequelize);

// Relations
db.Post.belongsTo(db.User, { as: 'postAuthor', foreignKey: 'author', sourceKey: 'ID' });
db.User.hasMany(db.Post, { as: 'posts', sourceKey: 'ID', foreignKey: 'author' });

// Many-Many relations

// Assoc_User_Badge
db.Badge.belongsToMany(db.User, {
  through: db.Assoc_User_Badge, as: 'users', sourceKey: 'ID', foreignKey: 'badgeRef',
});
db.User.belongsToMany(db.Badge, {
  through: db.Assoc_User_Badge, as: 'badges', sourceKey: 'ID', foreignKey: 'userRef',
});

// Assoc_User_Tag
db.Tag.belongsToMany(db.User, {
  through: db.Assoc_User_Tag, as: 'users', sourceKey: 'ID', foreignKey: 'tagRef',
});
db.User.belongsToMany(db.Tag, {
  through: db.Assoc_User_Tag, as: 'tags', sourceKey: 'ID', foreignKey: 'userRef',
});

// Assoc_Post_Tag
db.Tag.belongsToMany(db.Post, {
  through: db.Assoc_Post_Tag, as: 'posts', sourceKey: 'ID', foreignKey: 'tagRef',
});
db.Post.belongsToMany(db.Tag, {
  through: db.Assoc_Post_Tag, as: 'tags', sourceKey: 'ID', foreignKey: 'postRef',
});

// Test Assoc User Reaction Post
db.Post.belongsToMany(db.User, {
  through: db.Assoc_User_Reaction_Post, as: 'usersWhoReacted', sourceKey: 'ID', foreignKey: 'postRef',
});
db.User.belongsToMany(db.Post, {
  through: db.Assoc_User_Reaction_Post, as: 'reactedPosts', sourceKey: 'ID', foreignKey: 'userRef',
});

db.Post.belongsToMany(db.Reaction, {
  through: db.Assoc_User_Reaction_Post, as: 'reactions', sourceKey: 'ID', foreignKey: 'postRef',
});
db.Reaction.belongsToMany(db.Post, {
  through: db.Assoc_User_Reaction_Post, as: 'posts', sourceKey: 'ID', foreignKey: 'reactionRef',
});

// Authentication config
db.config = () => sequelize.authenticate()
  .then(() => console.log('Connected to database !'))
  .catch((error) => console.error('Unable to connect to the database:', error));

module.exports = db;
