const db = require('./db');

const { sequelize, User } = db;
// run db config
db.config();

// Try to find all users
User.findAll()
  .then((users) => console.log('All users:', JSON.stringify(users, null, 2)))
  .then(() => sequelize.close());
