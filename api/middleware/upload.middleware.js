const multer = require('multer');

// Extensions of files
const { extension } = require('mime-types');

/**
 * Storage for the templates images
 */
const templateStorage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'uploads/templates');
  },
  filename(req, file, cb) {
    cb(null, `${Date.now()}.${extension(file.mimetype)}`); // Appending extension
  },
});

/**
 * Middleware to manage template upload
 */
module.exports.uploadTemplate = multer({
  storage: templateStorage,
  fileFilter: (req, file, callback) => {
    const ext = extension(file.mimetype);
    if (ext !== 'png' && ext !== 'jpg' && ext !== 'jpeg' && ext !== 'gif') {
      return callback(new Error('Only images are allowed'));
    }
    callback(null, true);
  },
  // limits: {
  //   fileSize: 1024 * 1024,
  // },
});

/**
 * Storage for the profile images
 */
const profileStorage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'uploads/profile');
  },
  filename(req, file, cb) {
    cb(null, `${Date.now()}.${extension(file.mimetype)}`); // Appending extension
  },
});

/**
 * Middleware to manage profile image upload
 */
module.exports.uploadProfile = multer({
  storage: profileStorage,
  fileFilter: (req, file, callback) => {
    const ext = extension(file.mimetype);
    if (ext !== 'png' && ext !== 'jpg' && ext !== 'jpeg' && ext !== 'gif') {
      return callback(new Error('Only images are allowed'));
    }
    callback(null, true);
  },
  // limits: {
  //   fileSize: 1024 * 1024,
  // },
});
