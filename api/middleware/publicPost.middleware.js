const { Post } = require('../config/db');

/**
 * Middleware to check if a requested post is public
 * @param {number} id - In the request param, unique ID of the post
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 * @param {express.NextFunction} next - Go to the next middleware
 */
module.exports.checkPublicPost = (req, res, next) => {
  const { id } = req.params;
  Post.findByPk(id, {
    attributes: ['public'],
  })
    .then((post) => {
      if (post?.public) {
        next();
      } else {
        res.sendStatus(401);
      }
    })
    .catch((err) => {
      console.error(err);
      res.sendStatus(404);
    });
};
