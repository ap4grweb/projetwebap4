const jwt = require('jsonwebtoken');

// User model
const { User } = require('../config/db');

/**
 * Middleware to check if the user is logged in
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 * @param {express.NextFunction} next - Go to the next middleware
 */
module.exports.checkUser = (req, res, next) => {
  const token = req.cookies.jwt;

  if (token) {
    jwt.verify(token, process.env.TOKEN_SECRET, async (err, decodedToken) => {
      if (err) {
        res.locals.user = null;
        // Reset cookie if it malformed
        res.cookie('jwt', '', { maxAge: 1 });
        next();
      } else {
        // Find user by id and store it in cookie without password
        const user = await User.findByPk(decodedToken.id, { attributes: { exclude: ['password'] } })
          .catch(console.error);

        // Go to next
        res.locals.user = user;
        next();
      }
    });
  } else {
    // if no cookie --> no authentication
    res.locals.user = null;
    next();
  }
};

/**
 * Middleware to filter request without authentication
 * @param {express.Request} req - Request from the user
 * @param {express.Response} res - Response
 * @param {express.NextFunction} next - Go to the next middleware
 */
module.exports.requireAuth = (req, res, next) => {
  const token = req.cookies.jwt;

  /**
   * Function to refuse the connection
   */
  const refuse = () => res.status(200).json('No token !');

  if (token) {
    jwt.verify(token, process.env.TOKEN_SECRET, async (err, decodedToken) => {
      if (err) {
        console.log(err);
        refuse();
      } else {
        next();
      }
    });
  } else {
    console.log('No token !');
    refuse();
  }
};
