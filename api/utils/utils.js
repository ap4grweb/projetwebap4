const fs = require('fs');
const toStream = require('tostream');
const ExifTransformer = require('exif-be-gone');

/**
 * Encode image data to base64 encoded string
 * @param {string} imgPath - Path to the image relative to uploads folder 
 * @return {string} The image in base64
 */
module.exports.base64Encode = (imgPath) => fs.readFileSync(`${__dirname}/../uploads/${imgPath}`, 'base64');

/**
 * Delete an image on the server synchronously
 * @param {string} imgPath - Path to the image relative to uploads folder 
 * @return {void} 
 */
module.exports.deleteImg = (imgPath) => {
  try {
    fs.unlinkSync(`${__dirname}/../uploads/${imgPath}`);
  } catch (error) {
    console.log(error);
  }
};

/**
 * Delete all exifs data of an image
 * @param {string} imgPath - Path to the image relative to uploads folder 
 * @return {void}
 */
module.exports.deleteExifs = (imgPath) => {
  const isGif = new RegExp('.*\.gif');
  if (!isGif.exec(imgPath)) {
    try {
      const tempPath = `${imgPath}_`;

      const reader = fs.createReadStream(imgPath);
      const writer = fs.createWriteStream(tempPath);

      toStream(reader).pipe(new ExifTransformer()).pipe(writer);
      fs.renameSync(tempPath, imgPath);
    } catch (error) {
      console.log(error);
    }
  }
};
