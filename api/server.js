// Getting environnement variables
require('dotenv').config({ path: './config/.env' });

// Import libs and conf
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const db = require('./config/db.js');

// Import middleware
const { checkUser, requireAuth } = require('./middleware/auth.middleware');

// Import routers
const homeRouter = require('./routes/home.routes');
const imagesRouter = require('./routes/images.routes');
const usersRouter = require('./routes/users.routes');
const themesRouter = require('./routes/themes.routes');
const templatesRouter = require('./routes/templates.routes');
const postsRouter = require('./routes/posts.routes');
const tagsRouter = require('./routes/tags.routes');
const jwtRouter = require('./routes/jwt.routes');

// Express instance
const app = express();

// Cors
app.use(cors({
  origin: 'http://localhost:3000/',
}));

// Middleware
app.use(express.json());
app.use(cookieParser());
app.use(checkUser);

// Routes
// app.use('', homeRouter);
app.use('/images', imagesRouter);
app.use('/users', usersRouter);
app.use('/themes', requireAuth, themesRouter);
app.use('/templates', templatesRouter);
app.use('/posts', postsRouter);
app.use('/tags', tagsRouter);

// Jwt route to check authentication
app.use('/jwtid', jwtRouter);

// Serve front end
app.use(express.static('build'));
app.use((req, res) => {
  res.sendFile(`${__dirname}/build/index.html`);
});

// Launching server
const PORT = process.env.PORT || 5000;
db.config()
  .then(() => app.listen(PORT, () => {
    // eslint-disable-next-line no-console
    console.log(`Listening on http://localhost:${PORT}/`);
  }));
