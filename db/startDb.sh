#!/bin/bash

cat ./sql/db_creation.sql > setup.sql;
#cat ./sql/db_filling.sql >> setup.sql;
cat ./sql/dump.sql >> setup.sql;
docker build -t bdd . && docker run --name bdd_container -p 127.0.0.1:3306:3306 bdd &
