-- Initialisation of database
DROP DATABASE IF EXISTS captionYourMeme;
CREATE DATABASE captionYourMeme;
USE captionYourMeme;
-- Initialisation of table Theme
DROP TABLE IF EXISTS Theme;
CREATE TABLE Theme (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  file VARCHAR(50) NOT NULL,
  PRIMARY KEY(ID)
);
-- Initialisation of table Reaction
DROP TABLE IF EXISTS Reaction;
CREATE TABLE Reaction (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  value INT NOT NULL,
  isSuper boolean NOT NULL,
  PRIMARY KEY(ID)
);
-- Initialisation of table Tag
DROP TABLE IF EXISTS Tag;
CREATE TABLE Tag (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY(ID)
);
-- Initialisation of table Badge
DROP TABLE IF EXISTS Badge;
CREATE TABLE Badge (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(140) NOT NULL,
  img VARCHAR(50) NOT NULL,
  value INT NOT NULL,
  PRIMARY KEY(ID)
);
-- Initialisation of table Ranking
DROP TABLE IF EXISTS Ranking;
CREATE TABLE Ranking (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(140) NOT NULL,
  img VARCHAR(50) NOT NULL,
  threshold INT NOT NULL,
  PRIMARY KEY(ID)
);
-- Initialisation of table User
DROP TABLE IF EXISTS User;
CREATE TABLE User (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  username VARCHAR(50) NOT NULL,
  mail VARCHAR(50) NOT NULL,
  password VARCHAR(100) NOT NULL,
  bio VARCHAR(140),
  img VARCHAR(50),
  theme INT,
  registerTime DATETIME NOT NULL DEFAULT current_time,
  lastLoginTime DATETIME,
  lastFailedLogin DATETIME,
  loginAttemptCnt INT NOT NULL DEFAULT 0,
  superReactionsAvailable INT NOT NULL DEFAULT 0,
  PRIMARY KEY(ID),
  FOREIGN KEY (theme) REFERENCES Theme(ID) 
    ON DELETE CASCADE ON UPDATE CASCADE -- need to test
);
-- Initialisation of table Template
DROP TABLE IF EXISTS Template;
CREATE TABLE Template (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  img VARCHAR(50) NOT NULL,
  author INT NOT NULL,
  creationTime DATETIME NOT NULL DEFAULT current_time,
  PRIMARY KEY(ID),
  FOREIGN KEY (author) REFERENCES User(ID)
    ON DELETE CASCADE -- need to test
);

-- Initialisation of table Post
DROP TABLE IF EXISTS Post;
CREATE TABLE Post (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  template INT NOT NULL,
  author INT NOT NULL,
  caption VARCHAR(50) NOT NULL,
  public boolean NOT NULL,
  creationTime DATETIME NOT NULL DEFAULT current_time,
  PRIMARY KEY(ID),
  FOREIGN KEY (template) REFERENCES Template(ID) 
    ON DELETE CASCADE ON UPDATE CASCADE, -- need to test
  FOREIGN KEY (author) REFERENCES User(ID) 
    ON DELETE CASCADE ON UPDATE CASCADE -- need to test
);

-- Association tables

-- Initialise of association table User-Badge
DROP TABLE IF EXISTS Assoc_User_Badge;
CREATE TABLE Assoc_User_Badge
(
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  userRef INT NOT NULL,
  badgeRef INT NOT NULL,
  obtained DATETIME NOT NULL DEFAULT current_time,
  PRIMARY KEY(ID),
  FOREIGN KEY(userRef) REFERENCES User(ID)
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(badgeRef) REFERENCES Badge(ID)
    ON UPDATE CASCADE ON DELETE CASCADE

) ;

-- Initialise of association table Post-Tag
DROP TABLE IF EXISTS Assoc_Post_Tag;
CREATE TABLE Assoc_Post_Tag
(
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  postRef INT NOT NULL,
  tagRef INT NOT NULL,
  PRIMARY KEY(ID),
  FOREIGN KEY(postRef) REFERENCES Post(ID)
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(tagRef) REFERENCES Tag(ID)
    ON UPDATE CASCADE ON DELETE CASCADE

) ;

-- Initialise of association table User-Tag
DROP TABLE IF EXISTS Assoc_User_Tag;
CREATE TABLE Assoc_User_Tag
(
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  userRef INT NOT NULL,
  tagRef INT NOT NULL,
  PRIMARY KEY(ID),
  FOREIGN KEY(userRef) REFERENCES User(ID)
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(tagRef) REFERENCES Tag(ID)
    ON UPDATE CASCADE ON DELETE CASCADE

) ;


-- Initialise of association table User-Reaction-Post
DROP TABLE IF EXISTS Assoc_User_Reaction_Post;
CREATE TABLE Assoc_User_Reaction_Post
(
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  userRef INT NOT NULL,
  reactionRef INT NOT NULL,
  postRef INT NOT NULL,
  reactionTime DATETIME NOT NULL DEFAULT current_time,
  PRIMARY KEY(ID),
  FOREIGN KEY(userRef) REFERENCES User(ID)
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(reactionRef) REFERENCES Reaction(ID)
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(postRef) REFERENCES Post(ID)
    ON UPDATE CASCADE ON DELETE CASCADE

) ;--
-- Dumping data for table `Theme`
--

LOCK TABLES `Theme` WRITE;
/*!40000 ALTER TABLE `Theme` DISABLE KEYS */;
INSERT INTO `Theme` VALUES (1,'Dark Mode','dark.json'),(2,'Light Mode','light.json');
/*!40000 ALTER TABLE `Theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'jdoe','john.doe@example.com','$2b$10$lKlZC/qqSaZbH.plquuT1Ol.e6SqVSOsJL3D6qQ1vC346lQuVMI.C','My super bio','myProfile.jpg',1,'2021-05-19 00:08:13',NULL,NULL,0,0),(2,'luc321','luc.nels@example.com','$2b$10$lKlZC/qqSaZbH.plquuT1Ol.e6SqVSOsJL3D6qQ1vC346lQuVMI.C',NULL,NULL,1,'2021-05-19 00:08:13',NULL,NULL,0,0),(3,'carl311','carl.thoms@example.com','$2b$10$lKlZC/qqSaZbH.plquuT1Ol.e6SqVSOsJL3D6qQ1vC346lQuVMI.C',NULL,NULL,NULL,'2021-05-19 00:08:13',NULL,NULL,0,0);
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Badge`
--

LOCK TABLES `Badge` WRITE;
/*!40000 ALTER TABLE `Badge` DISABLE KEYS */;
INSERT INTO `Badge` VALUES (1,'Newbie','Badge earned when a new user registers','newbie.jpg',1),(2,'Normie','Badge earned by posting 10 memes using classic templates','normie.jpg',-2);
/*!40000 ALTER TABLE `Badge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Ranking`
--

LOCK TABLES `Ranking` WRITE;
/*!40000 ALTER TABLE `Ranking` DISABLE KEYS */;
INSERT INTO `Ranking` VALUES (1,'Apprentice good boy','On your way to become a good boy, keep it up, you need to have more than 10pts to qualify !','apprenticeGoodBoy.jpg',1),(2,'Good boy','Congratulation, you are a good boy !','goodBoy.jpg',10),(3,'Apprentice retard','On your way to become a basic turd, keep it up, you need to have less than -10pts to qualify !','apprenticeRetard.jpg',-1),(4,'Basic Turd','Bro, you really did it ?','basicTurd.jpg',-10);
/*!40000 ALTER TABLE `Ranking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Reaction`
--

LOCK TABLES `Reaction` WRITE;
/*!40000 ALTER TABLE `Reaction` DISABLE KEYS */;
INSERT INTO `Reaction` VALUES (1,'like',1,0),(2,'dislike',-1,0),(3,'superLike',5,1),(4,'superDislike',-5,1);
/*!40000 ALTER TABLE `Reaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Tag`
--

LOCK TABLES `Tag` WRITE;
/*!40000 ALTER TABLE `Tag` DISABLE KEYS */;
INSERT INTO `Tag` VALUES (1,'Popular'),(2,'IT'),(3,'Animals'),(4,'Sport'),(5,'gaming'),(6,'nsfw'),(7,'dsk'),(8,'memes'),(9,'trump'),(10,'yo'),(11,'no'),(12,'student'),(13,'random');
/*!40000 ALTER TABLE `Tag` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `Template`
--

LOCK TABLES `Template` WRITE;
/*!40000 ALTER TABLE `Template` DISABLE KEYS */;
INSERT INTO `Template` VALUES (1,'TrumpCeption','trump1234.jpg',1,'2021-05-19 00:08:13'),(2,'Calling The Police','callPolice2345.jpg',1,'2021-05-19 00:08:13'),(3,'Sponge Bob','bob54421.jpg',3,'2021-05-19 00:08:13'),(4,'want','1621890724334.jpeg',1,'2021-05-24 21:12:04'),(5,'homer','1621890760028.jpeg',1,'2021-05-24 21:12:40'),(6,'micheal','1621890781065.gif',1,'2021-05-24 21:13:01'),(7,'spiderman','1621890803024.jpeg',1,'2021-05-24 21:13:23'),(8,'heilarious','1621890817571.gif',1,'2021-05-24 21:13:37'),(9,'judge','1621890831805.gif',1,'2021-05-24 21:13:51'),(10,'michealstrokson','1621890855552.gif',1,'2021-05-24 21:14:15'),(11,'manofculutr','1621890880111.jpeg',1,'2021-05-24 21:14:40'),(12,'jimcarrey','1621890894765.gif',1,'2021-05-24 21:14:54'),(13,'butwhy','1621890911300.gif',1,'2021-05-24 21:15:11'),(14,'sully','1621890964960.gif',1,'2021-05-24 21:16:05'),(15,'bob','1621891007979.jpeg',1,'2021-05-24 21:16:47'),(16,'what','1621891202665.png',1,'2021-05-24 21:20:02'),(17,'headout','1621891225828.jpeg',1,'2021-05-24 21:20:25'),(18,'jerry','1621891265308.jpeg',1,'2021-05-24 21:21:05'),(19,'stupidhomer','1621891294942.jpeg',1,'2021-05-24 21:21:34'),(20,'wat','1621891339651.jpeg',1,'2021-05-24 21:22:19');
/*!40000 ALTER TABLE `Template` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `Post`
--

LOCK TABLES `Post` WRITE;
/*!40000 ALTER TABLE `Post` DISABLE KEYS */;
INSERT INTO `Post` VALUES (1,2,1,'When I forget my phone in the uber',1,'2021-05-19 00:08:13'),(2,3,3,'A few moments later',1,'2021-05-19 00:08:13'),(3,1,2,'Me when I go to mexico to eat a tacos',0,'2021-05-19 00:08:13'),(4,20,1,'when trap negotiate to reserve balls',1,'2021-05-24 21:22:45'),(5,10,1,'RE village GOTH when spotted say yum',1,'2021-05-24 21:25:06'),(6,5,1,'carlton hotel when they specify there storage',0,'2021-05-24 21:28:34'),(7,1,1,'conclude every evanescent hall with you',1,'2021-05-24 21:28:35'),(8,4,1,'isen ludicrous strategy for skillful students',1,'2021-05-24 21:29:36');
/*!40000 ALTER TABLE `Post` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `Assoc_User_Badge`
--

LOCK TABLES `Assoc_User_Badge` WRITE;
/*!40000 ALTER TABLE `Assoc_User_Badge` DISABLE KEYS */;
INSERT INTO `Assoc_User_Badge` VALUES (1,1,1,'2021-05-19 00:08:13'),(2,1,2,'2021-05-19 00:08:13'),(3,2,2,'2021-05-19 00:08:13'),(4,3,1,'2021-05-19 00:08:13');
/*!40000 ALTER TABLE `Assoc_User_Badge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Assoc_User_Reaction_Post`
--

LOCK TABLES `Assoc_User_Reaction_Post` WRITE;
/*!40000 ALTER TABLE `Assoc_User_Reaction_Post` DISABLE KEYS */;
INSERT INTO `Assoc_User_Reaction_Post` VALUES (1,1,1,1,'2021-05-19 00:08:13'),(2,1,2,3,'2021-05-19 00:08:13'),(3,2,2,2,'2021-05-19 00:08:13'),(4,3,1,3,'2021-05-19 00:08:13'),(5,1,2,2,'2021-05-24 21:37:30'),(6,1,1,4,'2021-05-24 21:37:34'),(7,1,1,7,'2021-05-24 21:37:55'),(8,1,1,6,'2021-05-24 21:38:04'),(9,1,1,8,'2021-05-24 21:38:18'),(10,1,1,5,'2021-05-24 21:38:24');
/*!40000 ALTER TABLE `Assoc_User_Reaction_Post` ENABLE KEYS */;
UNLOCK TABLES;

--

-- Dumping data for table `Assoc_User_Tag`
--

LOCK TABLES `Assoc_User_Tag` WRITE;
/*!40000 ALTER TABLE `Assoc_User_Tag` DISABLE KEYS */;
INSERT INTO `Assoc_User_Tag` VALUES (1,1,1),(2,1,2),(3,2,2),(4,3,1),(5,1,6),(6,1,5),(7,1,11),(8,1,10),(9,1,9);
/*!40000 ALTER TABLE `Assoc_User_Tag` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `Assoc_Post_Tag` WRITE;
/*!40000 ALTER TABLE `Assoc_Post_Tag` DISABLE KEYS */;
INSERT INTO `Assoc_Post_Tag` VALUES (1,1,1),(2,1,2),(3,2,2),(4,3,1),(5,5,5),(6,5,6),(7,6,7),(8,6,6),(9,6,8),(10,7,9),(11,7,10),(12,7,11),(13,8,12),(14,8,13);
/*!40000 ALTER TABLE `Assoc_Post_Tag` ENABLE KEYS */;
UNLOCK TABLES;



 