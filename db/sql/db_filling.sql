-- INSERTION OF data
USE captionYourMeme;

-- Theme
DELETE FROM Theme;
INSERT INTO
  Theme (name, file)
VALUES
  ("Dark Mode", "dark.json"),
  ("Light Mode", "light.json");
  
-- Reaction
DELETE FROM Reaction;
INSERT INTO
  Reaction (name, value, isSuper)
VALUES
  ("like", +1 , false),
  ("dislike", -1 , false),
  ("superLike", +5 , true),
  ("superDislike", -5 , true);

-- Tag
DELETE FROM Tag;
INSERT INTO
  Tag (name)
VALUES
  ("Popular"),
  ("IT"),
  ("Animals"),
  ("Sport");
  
-- Badge
DELETE FROM Badge;
INSERT INTO
  Badge (name, description, img, value)
VALUES
  ("Newbie", "Badge earned when a new user registers", "newbie.jpg" , 1),
  ("Normie", "Badge earned by posting 10 memes using classic templates" ,"normie.jpg" , -2);
  
-- Ranking
DELETE FROM Ranking;
INSERT INTO
  Ranking (name, description, img, threshold)
VALUES
  ("Apprentice good boy", "On your way to become a good boy, keep it up, you need to have more than 10pts to qualify !", "apprenticeGoodBoy.jpg" , 1),
  ("Good boy", "Congratulation, you are a good boy !", "goodBoy.jpg" , 10),
  ("Apprentice retard", "On your way to become a basic turd, keep it up, you need to have less than -10pts to qualify !", "apprenticeRetard.jpg" , -1),
  ("Basic Turd", "Bro, you really did it ?" ,"basicTurd.jpg" , -10);


-- User 
-- All pwd are Hello1234+
DELETE FROM User;
INSERT INTO 
  User (username, mail, password, bio, img, theme)
VALUES
  ("jdoe", "john.doe@example.com", "$2b$10$lKlZC/qqSaZbH.plquuT1Ol.e6SqVSOsJL3D6qQ1vC346lQuVMI.C", "My super bio", "myProfile.jpg", 1),
  ("luc321", "luc.nels@example.com", "$2b$10$lKlZC/qqSaZbH.plquuT1Ol.e6SqVSOsJL3D6qQ1vC346lQuVMI.C", NULL, NULL, 1),
  ("carl311", "carl.thoms@example.com", "$2b$10$lKlZC/qqSaZbH.plquuT1Ol.e6SqVSOsJL3D6qQ1vC346lQuVMI.C", NULL, NULL, NULL);
  
-- Template
DELETE FROM Template;
INSERT INTO 
  Template (name, img, author)
VALUES
  ("TrumpCeption", "trump1234.jpg", 1),
  ("Calling The Police", "callPolice2345.jpg", 1),
  ("Sponge Bob", "bob54421.jpg", 3);
  
  
-- Post
DELETE FROM Post;
INSERT INTO 
  Post (template, author, caption, public)
VALUES
  (2, 1,"When I forget my phone in the uber", true),
  (3, 3,"A few moments later", true),
  (1, 2,"Me when I go to mexico to eat a tacos", false);
  

-- Assoc_User_Badge
DELETE FROM Assoc_User_Badge;
INSERT INTO 
  Assoc_User_Badge (userRef,badgeRef)
VALUES
  (1,1),
  (1,2),
  (2,2),
  (3,1);
  
-- Assoc_Post_Tag
DELETE FROM Assoc_Post_Tag;
INSERT INTO 
  Assoc_Post_Tag (postRef,tagRef)
VALUES
  (1,1),
  (1,2),
  (2,2),
  (3,1);
  
-- Assoc_User_Tag
DELETE FROM Assoc_User_Tag;
INSERT INTO 
  Assoc_User_Tag (userRef,tagRef)
VALUES
  (1,1),
  (1,2),
  (2,2),
  (3,1);
  
-- Assoc_User_Tag
DELETE FROM Assoc_User_Reaction_Post;
INSERT INTO 
  Assoc_User_Reaction_Post (userRef,reactionRef,postRef)
VALUES
  (1,1,1),
  (1,2,3),
  (2,2,2),
  (3,1,3);