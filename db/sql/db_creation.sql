-- Initialisation of database
DROP DATABASE IF EXISTS captionYourMeme;
CREATE DATABASE captionYourMeme;
USE captionYourMeme;
-- Initialisation of table Theme
DROP TABLE IF EXISTS Theme;
CREATE TABLE Theme (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  file VARCHAR(50) NOT NULL,
  PRIMARY KEY(ID)
);
-- Initialisation of table Reaction
DROP TABLE IF EXISTS Reaction;
CREATE TABLE Reaction (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  value INT NOT NULL,
  isSuper boolean NOT NULL,
  PRIMARY KEY(ID)
);
-- Initialisation of table Tag
DROP TABLE IF EXISTS Tag;
CREATE TABLE Tag (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY(ID)
);
-- Initialisation of table Badge
DROP TABLE IF EXISTS Badge;
CREATE TABLE Badge (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(140) NOT NULL,
  img VARCHAR(50) NOT NULL,
  value INT NOT NULL,
  PRIMARY KEY(ID)
);
-- Initialisation of table Ranking
DROP TABLE IF EXISTS Ranking;
CREATE TABLE Ranking (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(140) NOT NULL,
  img VARCHAR(50) NOT NULL,
  threshold INT NOT NULL,
  PRIMARY KEY(ID)
);
-- Initialisation of table User
DROP TABLE IF EXISTS User;
CREATE TABLE User (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  username VARCHAR(50) NOT NULL,
  mail VARCHAR(50) NOT NULL,
  password VARCHAR(100) NOT NULL,
  bio VARCHAR(140),
  img VARCHAR(50),
  theme INT,
  registerTime DATETIME NOT NULL DEFAULT current_time,
  lastLoginTime DATETIME,
  lastFailedLogin DATETIME,
  loginAttemptCnt INT NOT NULL DEFAULT 0,
  superReactionsAvailable INT NOT NULL DEFAULT 0,
  PRIMARY KEY(ID),
  FOREIGN KEY (theme) REFERENCES Theme(ID) 
    ON DELETE CASCADE ON UPDATE CASCADE -- need to test
);
-- Initialisation of table Template
DROP TABLE IF EXISTS Template;
CREATE TABLE Template (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  img VARCHAR(50) NOT NULL,
  author INT NOT NULL,
  creationTime DATETIME NOT NULL DEFAULT current_time,
  PRIMARY KEY(ID),
  FOREIGN KEY (author) REFERENCES User(ID)
    ON DELETE CASCADE -- need to test
);

-- Initialisation of table Post
DROP TABLE IF EXISTS Post;
CREATE TABLE Post (
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  template INT NOT NULL,
  author INT NOT NULL,
  caption VARCHAR(50) NOT NULL,
  public boolean NOT NULL,
  creationTime DATETIME NOT NULL DEFAULT current_time,
  PRIMARY KEY(ID),
  FOREIGN KEY (template) REFERENCES Template(ID) 
    ON DELETE CASCADE ON UPDATE CASCADE, -- need to test
  FOREIGN KEY (author) REFERENCES User(ID) 
    ON DELETE CASCADE ON UPDATE CASCADE -- need to test
);

-- Association tables

-- Initialise of association table User-Badge
DROP TABLE IF EXISTS Assoc_User_Badge;
CREATE TABLE Assoc_User_Badge
(
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  userRef INT NOT NULL,
  badgeRef INT NOT NULL,
  obtained DATETIME NOT NULL DEFAULT current_time,
  PRIMARY KEY(ID),
  FOREIGN KEY(userRef) REFERENCES User(ID)
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(badgeRef) REFERENCES Badge(ID)
    ON UPDATE CASCADE ON DELETE CASCADE

) ;

-- Initialise of association table Post-Tag
DROP TABLE IF EXISTS Assoc_Post_Tag;
CREATE TABLE Assoc_Post_Tag
(
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  postRef INT NOT NULL,
  tagRef INT NOT NULL,
  PRIMARY KEY(ID),
  FOREIGN KEY(postRef) REFERENCES Post(ID)
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(tagRef) REFERENCES Tag(ID)
    ON UPDATE CASCADE ON DELETE CASCADE

) ;

-- Initialise of association table User-Tag
DROP TABLE IF EXISTS Assoc_User_Tag;
CREATE TABLE Assoc_User_Tag
(
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  userRef INT NOT NULL,
  tagRef INT NOT NULL,
  PRIMARY KEY(ID),
  FOREIGN KEY(userRef) REFERENCES User(ID)
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(tagRef) REFERENCES Tag(ID)
    ON UPDATE CASCADE ON DELETE CASCADE

) ;


-- Initialise of association table User-Reaction-Post
DROP TABLE IF EXISTS Assoc_User_Reaction_Post;
CREATE TABLE Assoc_User_Reaction_Post
(
  ID INT UNIQUE NOT NULL AUTO_INCREMENT,
  userRef INT NOT NULL,
  reactionRef INT NOT NULL,
  postRef INT NOT NULL,
  reactionTime DATETIME NOT NULL DEFAULT current_time,
  PRIMARY KEY(ID),
  FOREIGN KEY(userRef) REFERENCES User(ID)
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(reactionRef) REFERENCES Reaction(ID)
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(postRef) REFERENCES Post(ID)
    ON UPDATE CASCADE ON DELETE CASCADE

) ;