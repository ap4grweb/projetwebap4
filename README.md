# ProjetWebAp4

This repo contains an image based social network platform.

For now it does not have a specific name, we simply call it **ProjetWebAp4**.

## Concept

In short: we wanted to innovate.

The platform revolves around memes, it's a meme sharing platform.

A post consists of a template (the image), a caption and some tags.

By creating an account, you will be able to explore the memes as well as make some.

Our editor gives you the choice between a few templates, and if you do not find what you're looking for, you can still upload new images.
The captions are partially auto-generated, you have to associate words with each others to make a sentence, you can also use up to 3 custom words to make your sentence more meaningful.

A post's tags is what sorts the posts into the different categories.
As a user you can follow multiple tags and view them in your feed.

By posting content and interracting on the platform, you earn badges, these badges make you earn a rank that you can use to show the quality of your content.

## Installation

You need **Docker** and **docker-compose** to run this project.

```bash
docker-compose --build

```
By default the port is 80, you can edit it in **docker-compose.yml**

(Or you can just go to https://captionyourmeme.com/)

## Contributing
Any help is very welcome, if you want to contribute contact us first, see contact info below.

## License
[Creative Commons (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/)

## Contact
louis.naili@student.junia.com

tristan.favrel@student.junia.com

nathan.fourrier@student.junia.com
