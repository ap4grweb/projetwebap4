export const finalPost = {
  ID: -1,
  template: -1,
  author: -1,
  caption: "You've reached the end of this feed, come back later for more...",
  public: -1,
  creationTime: '-1',
  tags: [],
};

export const finalTemplate = {
  ID: -1,
  name: 'Nothing to see here...',
  img: 'images/templates/NTSH.jpg',
  author: -1,
  creationTime: '-1',
};
