const customPalette = {
  palette: {
    type: 'light',
    primary: {
      main: '#03A9F4',
      light: '#35BAF6',
      contrastText: '#ffffff',
    },
    secondary: {
      main: '#009688',
      contrastText: '#ffffff',
    },
    error: {
      main: '#FF5722',
    },
    warning: {
      main: '#FF9800',
    },
    info: {
      main: '#CDDC39',
    },
    success: {
      main: '#00BCD4',
    },
    contrastThreshold: 3,
    tonalOffset: 0.2,
  },
};
export default customPalette;
