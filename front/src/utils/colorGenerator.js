const colors = ['primary', 'secondary'];

const getRandomColour = () => colors[Math.floor(Math.random() * colors.length)];

export default getRandomColour;
