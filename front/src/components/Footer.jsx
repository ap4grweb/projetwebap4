import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles, Link } from '@material-ui/core';

const Footer = () => {
  const useStyles = makeStyles(() => ({
    link: {
      textDecoration: 'underline',
    },
  }));
    // Apply style
  const classes = useStyles();
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      <Link color="inherit" href="https://gitlab.com/ap4grweb/projetwebap4" className={classes.link}>
        Join us on Gitlab !
      </Link>
    </Typography>
  );
};
export default Footer;
