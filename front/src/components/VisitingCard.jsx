import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import { getPostScore, getPostAuthor } from '../api';

const useStyles = makeStyles((theme) => ({
  avatar: {
    backgroundColor: theme.palette.primary.main,
  },
}));

const VisitingCard = ({ postId }) => {
  const classes = useStyles();
  const [author, setAuthor] = useState('');
  const [score, setScore] = useState(0);

  useEffect(() => {
    if (postId !== '-1' && postId !== undefined) {
      getPostAuthor(postId)
        .then(({ username: value }) => setAuthor(value))
        .catch(console.error);

      getPostScore(postId)
        .then(({ score: value }) => setScore(value))
        .catch(console.error);
    }
  }, []);

  return (
    <>
      {
      (author === '') ? ''
        : (
          <CardHeader
            avatar={(
              <Avatar aria-label="recipe" className={classes.avatar}>
                {author ? author.charAt(0).toUpperCase() : ''}
              </Avatar>
        )}
            title={`Author: ${author} | Score : ${score}`}
            subheader=""
          />
        )
}
    </>
  );
};

VisitingCard.propTypes = {
  postId: PropTypes.string.isRequired,
};

export default VisitingCard;
