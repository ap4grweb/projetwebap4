// React imports
import React from 'react';
import PropTypes from 'prop-types';

// Libs
import TinderCard from 'react-tinder-card';
import { makeStyles } from '@material-ui/core/styles';
import {
  Card, Container, CardContent,
} from '@material-ui/core/';
import VisitingCard from '../VisitingCard';

// Component Swiper
const Swiper = ({
  reference,
  postId, img, caption, locked, onLike, onDislike, onSuperLike, onSuperDislike, onSwipeEnd,
}) => {
  // Stylesheet
  const useStyles = makeStyles({
    swiperContainer: {
      padding: 'none',
      width: '100%',
      height: '100%',
    },
    tinderCard: {
      backgroundColor: 'rgba(0,0,0,0.05)',
      borderRadius: '20px',
      overflow: 'hidden',
      transition: 'all 0.25s ease-out',
      '&:hover': {
        backgroundColor: 'rgba(0,0,0,0.1)',
      },
    },
    noBg: {
      boxShadow: 'none',
      background: 'none',
    },
    swiperImage: {
      width: '100%',
      height: '45vh',
      objectFit: 'contain',
    },
    swiperCaption: {
      margin: '0',
      padding: '10px',
      textAlign: 'center',
      fontWeight: 'bold',
    },
  });

  // Apply styles
  const classes = useStyles();

  const onSwipe = (direction) => {
    if (!locked) {
      switch (direction) {
        case 'left':
          onLike();
          break;
        case 'right':
          onDislike();
          break;
        case 'up':
          onSuperLike();
          break;
        case 'down':
          onSuperDislike();
          break;
        default:
          break;
      }
    }
  };

  return (
    <Container className={classes.swiperContainer}>
      <TinderCard
        className={classes.tinderCard}
        ref={reference}
        onSwipe={onSwipe}
        onCardLeftScreen={onSwipeEnd}
        preventSwipe={locked ? ['left', 'right', 'up', 'down'] : []}
      >
        <VisitingCard
          className={classes.noBg}
          postId={(postId || -1).toString()}
        />
        <Card className={classes.noBg}>
          <img
            className={classes.swiperImage}
            alt={caption}
            src={img}
          />
          <CardContent className={classes.swiperCaption}>
            {caption}
          </CardContent>
        </Card>
      </TinderCard>
    </Container>
  );
};

// Props default values
Swiper.defaultProps = {
  img: '',
  postId: '',
  caption: '',

};

// Types checking
Swiper.propTypes = {
  reference: PropTypes.shape({}).isRequired,
  postId: PropTypes.string,
  img: PropTypes.string,
  caption: PropTypes.string,
  locked: PropTypes.bool,
  onLike: PropTypes.func.isRequired,
  onDislike: PropTypes.func.isRequired,
  onSuperLike: PropTypes.func.isRequired,
  onSuperDislike: PropTypes.func.isRequired,
  onSwipeEnd: PropTypes.func.isRequired,
};

Swiper.defaultProps = {
  locked: false,
};

export default Swiper;
