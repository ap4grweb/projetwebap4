// React imports
import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

// Material UI
import {
  Button, Container, LinearProgress, makeStyles,
} from '@material-ui/core/';
import {
  ThumbUp, ThumbDown, Favorite, NotInterested,
} from '@material-ui/icons';

// Local
import { getTemplate, putReaction } from '../../api';
import Swiper from './Swiper';
import { finalPost, finalTemplate } from '../../utils/finalPost';

// Components
const MySwiper = ({ getFunction }) => {
// Refs
  const swiperRef = useRef();

  // States
  const [post, setPost] = useState();
  const [templateImg, setTemplateImg] = useState();
  const [swiperKey, setSwiperKey] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [lock, setLock] = useState(false);

  // Stylesheet
  const useStyles = makeStyles((theme) => ({
    progressBar: {
      display: isLoading ? 'block' : 'none',
      opacity: '0.5',
      width: '100%',
    },
    postContainer: {
      display: isLoading ? 'none' : 'block',
      margin: 'auto',
      [theme.breakpoints.up('sm')]: {
        width: '100%',
        padding: '0px',
      },
      [theme.breakpoints.up('md')]: {
        width: '80%',
        padding: '0px',
      },
      [theme.breakpoints.up('lg')]: {
        paddingTop: '20px',
        width: '60%',
      },
    },
    buttons: {
      display: 'flex',
      justifyContent: 'space-between',
      marginTop: '10px',
      maxWidth: '500px',
    },
    likeBtn: {
      background: theme.palette.primary,
      '&:hover': {
        background: theme.palette.primary.light,
      },
    },
    dislikeBtn: {
      background: theme.palette.error.main,
      '&:hover': {
        background: theme.palette.error.light,
      },
    },
    superLikeBtn: {
      background: theme.palette.secondary.main,
      '&:hover': {
        background: theme.palette.secondary.light,
      },
    },
    superDislikeBtn: {
      background: theme.palette.warning.main,
      '&:hover': {
        background: theme.palette.warning.light,
      },
    },
  }));

  // Apply styles
  const classes = useStyles();

  // Fetching user
  const fetchPost = async () => {
    const newPost = await getFunction();
    if (newPost === '') {
      setPost(finalPost);
      setTemplateImg(finalTemplate);
      setLock(true);
    } else {
      setPost(newPost);
      const newTemplate = await getTemplate(newPost.template);
      setTemplateImg(newTemplate);
      setLock(false);
    }
    setIsLoading(false);
    setSwiperKey((prev) => prev + 1);
  };

  // Handle swiper actions
  const handleLike = () => {
    putReaction(post.ID, 'like')
      .catch(console.error);
  };

  const handleDislike = () => {
    putReaction(post.ID, 'dislike')
      .catch(console.error);
  };

  const handleSuperLike = () => {
    putReaction(post.ID, 'superLike')
      .catch(console.error);
  };

  const handleSuperDislike = () => {
    putReaction(post.ID, 'superDislike')
      .catch(console.error);
  };

  const handleSwipeEnd = () => {
    setIsLoading(true);
    fetchPost()
      .catch(console.error);
  };

  // Use effects
  useEffect(fetchPost, []);

  return (
    <>
      <LinearProgress className={classes.progressBar} />
      <div className={classes.postContainer}>
        <Swiper
          key={swiperKey}
          reference={swiperRef}
          postId={post?.ID}
          img={templateImg?.img}
          caption={post?.caption}
          locked={lock}
          onLike={handleLike}
          onDislike={handleDislike}
          onSuperLike={handleSuperLike}
          onSuperDislike={handleSuperDislike}
          onSwipeEnd={handleSwipeEnd}
        />
        <Container className={classes.buttons}>
          {/* SuperLike button */}
          <Button className={classes.superLikeBtn} variant="contained" color="primary" onClick={lock ? () => {} : () => swiperRef.current.swipe('up')}>
            <Favorite />
          </Button>
          {/* Like Button */}
          <Button className={classes.likeBtn} variant="contained" color="primary" onClick={lock ? () => {} : () => swiperRef.current.swipe('left')}>
            <ThumbUp />
          </Button>
          {/* Dislike button */}
          <Button className={classes.dislikeBtn} variant="contained" color="primary" onClick={lock ? () => {} : () => swiperRef.current.swipe('right')}>
            <ThumbDown />
          </Button>
          {/* SuperDislike button */}
          <Button className={classes.superDislikeBtn} variant="contained" color="primary" onClick={lock ? () => {} : () => swiperRef.current.swipe('down')}>
            <NotInterested />
          </Button>
        </Container>
      </div>
    </>
  );
};

MySwiper.propTypes = {
  getFunction: PropTypes.func.isRequired,
};

export default MySwiper;
