// React imports
import React, { useState } from 'react';

// Material UI
import { makeStyles, Button } from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

// Local
import CreatePost from './create/create-post/CreatePost';
import CreateTemplate from './create/create-template/CreateTemplate';
import ChooseTemplate from './create/find-template/ChooseTemplate';

// I dont remember
const CreateView = () => {
  const useStyles = makeStyles(() => ({
    backIcon: {
      fontSize: '3rem',
      marginTop: '40px',
      position: 'absolute',
    },
  }));

  // Apply style
  const classes = useStyles();
  const [view, setView] = useState('choose-template');
  const [template, setTemplate] = useState();

  return (
    <div>
      {view !== 'choose-template'
     && (
     <Button onClick={() => setView('choose-template')}>
       <ArrowBackIosIcon className={classes.backIcon} />
     </Button>
     )}
      {view === 'choose-template' && (
      <ChooseTemplate
        goTemplateCreation={() => setView('create-template')}
        goPostCreation={(temp) => {
          setTemplate(temp);
          setView('create-post');
        }}
      />
      )}
      {view === 'create-template' && (
      <CreateTemplate goPostCreation={(temp) => {
        setTemplate(temp);
        setView('create-post');
      }}
      />
      )}
      {view === 'create-post' && (
      <CreatePost
        template={template}
        goTemplateChoose={() => setView('choose-template')}
      />
      )}
    </div>
  );
};

export default CreateView;
