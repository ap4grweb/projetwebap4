// React imports
import React, { useState, useEffect } from 'react';

// Material UI
import {
  makeStyles, Button,
} from '@material-ui/core/';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

// Local
import { getUserId, getUser } from '../../api';
import Profile from './account/Profile';
import HistoryPostList from './account/HistoryPostList';
import EditProfile from './account/EditProfile';

// Component AccountView
const AccountView = () => {
  // Stylesheet
  const useStyles = makeStyles(() => ({
    backIcon: {
      position: 'absolute',
      marginTop: '40px',
      fontSize: '3rem',
    },
  }));

  // Apply style
  const classes = useStyles();

  // States
  const [user, setUser] = useState();
  const [userID, setUserID] = useState('');

  // view
  const [view, setView] = useState('profile');

  const fetchUser = async () => {
    const id = await getUserId();
    setUserID(id);
    if (id !== '') {
      const userObject = await getUser(id);
      setUser(userObject);
    } else { setUser(''); }
  };
  useEffect(fetchUser, []);

  return (
    <div className={classes.root}>
      {view !== 'profile'
     && (
     <Button onClick={() => setView('profile')}>
       <ArrowBackIosIcon className={classes.backIcon} />
     </Button>
     )}

      {view === 'profile' && user && (
      <Profile
        user={user}
        userID={userID}
        goHistory={() => setView('post-history')}
        goMyPosts={() => setView('my-posts')}
        goEditProfile={() => setView('edit-profile')}
      />
      )}
      {view === 'post-history' && (
        <HistoryPostList isReaction />
      )}
      {view === 'my-posts' && (
        <HistoryPostList />

      )}
      {view === 'edit-profile' && (
        <EditProfile
          user={user}
          userID={userID}
          onUpdate={() => fetchUser()
            .then(() => setView('profile'))}
        />

      )}
    </div>
  );
};
export default AccountView;
