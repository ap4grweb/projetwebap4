// React imports
import React, { useState, useEffect } from 'react';

// Material UI
import {
  makeStyles, Button, Container, OutlinedInput,
} from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

// Local
import { getTags } from '../../api';
import CategoryFeed from './explore/CategoryFeed';

// Component ExploreView
const ExploreView = () => {
  // Stylesheet
  const useStyles = makeStyles((theme) => ({
    header: {
      position: 'absolute',
    },
    backIcon: {
      fontSize: '3rem',
    },
    tagList: {
      textAlign: 'center',
      display: 'inline-block',
    },
    tagBtn: {
      height: '40px',
      margin: '10px',
      background: theme.palette.primary,
      '&:hover': {
        background: theme.palette.primary.light,
      },
    },
    container: {
      textAlign: 'center',
    },
  }));

  // Use stylesheet
  const classes = useStyles();

  // States
  const [search, setSearch] = useState('');
  const [searchMode, setSearchMode] = useState(true);
  const [selectedTag, setSelectedTag] = useState('');
  const [tagList, setTagList] = useState([search]);

  // Functions
  const getTagList = () => {
    getTags()
      .then((tags) => setTagList([...tags]));
  };

  const handleSearch = (e) => {
    const newVal = e.target.value;
    setSearch(newVal);
  };

  const visitTag = (tag) => {
    setSelectedTag(tag);
    setSearchMode(false);
  };

  // UseEffects
  useEffect(getTagList, []);

  return (
    <>
      <div className={classes.container}>
        <div className={classes.header}>
          {!searchMode && (
          <Button onClick={() => setSearchMode(true)}>
            <ArrowBackIosIcon className={classes.backIcon} />
          </Button>
          )}
        </div>
        {searchMode
          ? (
            <Container className={classes.tagList}>
              <div>
                <OutlinedInput
                  fullWidth
                  placeholder="Find posts associated with the specified tags"
                  value={search}
                  onChange={handleSearch}
                />

              </div>
              <Button
                className={classes.tagBtn}
                color="primary"
                variant="contained"
                onClick={() => visitTag(search)}
              >
                #
                {search}
              </Button>
              {tagList.map((tag) => (
                <Button
                  className={classes.tagBtn}
                  key={tag.ID}
                  color="primary"
                  variant="contained"
                  onClick={() => visitTag(tag.name)}
                >
                  #
                  {tag.name}
                </Button>
              ))}
            </Container>
          )
          : <CategoryFeed tag={selectedTag} goBack={() => setSearchMode(true)} />}
      </div>
    </>
  );
};

export default ExploreView;
