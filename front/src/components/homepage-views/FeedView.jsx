// React imports
import React from 'react';

// Local
import { getFeed } from '../../api';
import MySwiper from '../swiper/mySwiper';

// Component FeedView
const FeedView = () => (
  <>
    <MySwiper getFunction={getFeed} />
  </>
);

export default FeedView;
