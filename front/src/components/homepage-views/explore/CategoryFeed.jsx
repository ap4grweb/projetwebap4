import React from 'react';
import PropTypes from 'prop-types';
import { getPostByTag } from '../../../api';
import MySwiper from '../../swiper/mySwiper';

const CategoryFeed = ({ tag }) => (
  <>
    <MySwiper getFunction={() => getPostByTag(tag)} />
  </>
);

CategoryFeed.propTypes = {
  tag: PropTypes.string.isRequired,
};

export default CategoryFeed;
