// React imports
import React from 'react';
import PropTypes from 'prop-types';
// Material UI
import {
  makeStyles, Card, Button, Container, CardHeader,
} from '@material-ui/core';

// Component HistoryCard
const HistoryCard = ({ goHistory, goMyPosts }) => {
  // Stylesheet
  const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 500,
      margin: '20px',
    },
    card: {
      height: 250,
    },
    cardAction: {
      height: '70%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      alignItems: 'center',
    },
    button: {
      width: '70%',
      height: '50px',
      background: theme.palette.primary.main,
      '&:hover': {
        background: theme.palette.primary.light,
      },
    },
  }));

  // Use stylesheet
  const classes = useStyles();

  return (
    <Container className={classes.root}>
      <Card className={classes.card}>
        <CardHeader
          title="Posts"
        />
        <div className={classes.cardAction}>
          <Button
            className={classes.button}
            size="small"
            color="primary"
            variant="contained"
            onClick={goHistory}
          >
            My reaction history
          </Button>
          <Button
            className={classes.button}
            size="small"
            color="primary"
            variant="contained"
            onClick={goMyPosts}
          >
            My posts
          </Button>
        </div>
      </Card>
    </Container>
  );
};

// Types checking
HistoryCard.propTypes = {
  goHistory: PropTypes.func.isRequired,

  goMyPosts: PropTypes.func.isRequired,
};

export default HistoryCard;
