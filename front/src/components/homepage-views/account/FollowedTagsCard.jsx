// React imports
import React, { useState, useEffect } from 'react';

// Material UI
import {
  makeStyles, Card, ListItem, Checkbox, Container, List, ListItemIcon, ListItemText, CardHeader,
} from '@material-ui/core';

// Local
import {
  getTags, getMyTags, addToMyTags, removeFromMyTags,
} from '../../../api';

// Component FollowedTagsCard
const FollowedTagsCard = () => {
  // Stylesheet
  const useStyles = makeStyles({
    root: {
      maxWidth: 500,
      margin: '20px',
    },
    card: {
      display: 'flex',
      flexFlow: 'column',
      height: '250px',
    },
    list: {
      position: 'relative',
      alignItems: 'center',
      overflowY: 'scroll',
      flex: '1 1 auto',
    },
  });

  // Use Stylesheet
  const classes = useStyles();

  // States
  const [tagList, setTagList] = useState([]);
  const [myTagList, setMyTagList] = useState([]);

  // Functions
  const getTagList = () => {
    getTags()
      .then((tags) => setTagList([...tags]));
    getMyTags()
      .then((tags) => setMyTagList([...tags]));
  };

  const handleCheck = (e, tag) => {
    if (e.target.checked) {
      addToMyTags({ tag })
        .then(getTagList);
    } else {
      removeFromMyTags({ tag })
        .then(getTagList);
    }
  };

  // Use effects
  useEffect(getTagList, []);

  return (
    <Container className={classes.root}>
      <Card className={classes.card}>
        <CardHeader title="Tags I follow" />
        <List className={classes.list}>
          {tagList.map((tag) => (
            <ListItem key={tag.ID} role={undefined} dense button>
              <ListItemIcon>
                <Checkbox
                  color="primary"
                  edge="start"
                  disableRipple
                  onChange={(e) => handleCheck(e, tag.name)}
                  checked={myTagList.findIndex((elt) => elt.name === tag.name) !== -1}
                />
              </ListItemIcon>
              <ListItemText id={tag.ID} primary={tag.name} />
            </ListItem>
          ))}
        </List>
      </Card>
    </Container>
  );
};

export default FollowedTagsCard;
