// React imports
import React from 'react';
import PropTypes from 'prop-types';

// Material UI
import {
  makeStyles, Card, CardActionArea, CardMedia, CardContent, Typography, Container, CardActions,
  Button,
} from '@material-ui/core';

// Component UserCard
const UserCard = ({
  profilePic, username, bio, goEditProfile,
}) => {
  // Stylesheet
  const useStyles = makeStyles({
    root: {
      maxWidth: '500px',
      margin: '20px',
    },
    card: {
      height: '250px',
    },
    media: {
      height: '110px',
    },
  });

  // Apply style
  const classes = useStyles();

  return (
    <Container className={classes.root}>
      <Card className={classes.card}>
        <CardActionArea onClick={goEditProfile}>
          <CardMedia
            className={classes.media}
            image={profilePic}
            title={username}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {username}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {bio}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary" onClick={goEditProfile}>Click to edit</Button>
        </CardActions>
      </Card>
    </Container>
  );
};

// Types checking
UserCard.propTypes = {
  profilePic: PropTypes.string.isRequired,

  username: PropTypes.string.isRequired,

  bio: PropTypes.string.isRequired,

  goEditProfile: PropTypes.func.isRequired,
};

export default UserCard;
