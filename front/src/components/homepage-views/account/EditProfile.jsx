// React imports
import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';

// Material UI
import {
  makeStyles, Button, TextField, Card, InputLabel, Container,
} from '@material-ui/core';
import PublishIcon from '@material-ui/icons/Publish';

// Local
import { patchPofile } from '../../../api';

// Component EditProfile
const EditProfile = ({ user, userID, onUpdate }) => {
  // Stylesheet
  const useStyles = makeStyles((theme) => ({
    container: {
      maxWidth: 600,
      margin: 'auto',
    },
    card: {
      height: '300px',
    },
    media: {
      width: '100%',
      height: '100%',
      objectFit: 'contain',
    },
    button: {
      background: theme.palette.primary.main,
      '&:hover': {
        background: theme.palette.primary.light,
      },
    },
  }));

  // Use stylesheet
  const classes = useStyles();

  // States
  const [bio, setBio] = useState(user.bio);
  const [img, setImg] = useState({ src: `images/profile/${user.img}` });
  const fileInputRef = useRef();

  // Functions
  const handleFileChange = () => {
    const imageFile = fileInputRef.current.files[0];

    const reader = new FileReader();
    reader.onload = function (e) {
      const image = document.createElement('img');
      image.src = e.target.result;
      setImg(image);
    };
    reader.readAsDataURL(imageFile);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData();
    const imageFile = fileInputRef.current.files[0];
    formData.append('file', imageFile);
    formData.append('bio', bio);

    patchPofile(userID, formData)
      .then(onUpdate)
      .catch(console.error);
  };

  return (
    <Container className={classes.container}>

      <form className={classes.form} noValidate onSubmit={handleSubmit}>

        {/* Upload button */}
        <InputLabel htmlFor="browse-input">
          <Card
            className={classes.card}
          >
            {
              (img.src !== 'images/profile/')
                ? (
                  <img
                    className={classes.media}
                    alt="Profile pic"
                    src={img.src}
                  />
                )
                : <PublishIcon className={classes.media} />
            }
          </Card>

        </InputLabel>
        <input
          ref={fileInputRef}
          style={{ display: 'none' }}
          accept="image/*"
          type="file"
          onChange={handleFileChange}
          id="browse-input"
        />

        {/* Bio of the user  */}
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="bio"
          type="text"
          multiline
          label="Bio"
          value={bio}
          onChange={(e) => (setBio(e.target.value))}
          name="bio"
        />

        <Button
          className={classes.button}
          type="submit"
          variant="contained"
          color="primary"
          fullWidth
        >
          Submit
        </Button>

      </form>
    </Container>
  );
};

// Types checking
EditProfile.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string,
    bio: PropTypes.string,
    img: PropTypes.string,
  }).isRequired,

  userID: PropTypes.number.isRequired,

  onUpdate: PropTypes.func.isRequired,
};

export default EditProfile;
