// React imports
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

// Material UI
import {
  makeStyles,
} from '@material-ui/core/';
import { getUserBadges } from '../../../api';

// Local
import UserCard from './UserCard';
import BadgesList from './BadgesList';
import HistoryCard from './HistoryCard';
import LeaveCard from './LeaveCard';
import FollowedTagsCard from './FollowedTagsCard';

// Component Profile
const Profile = ({
  user, userID, goHistory, goMyPosts, goEditProfile,
}) => {
  // Stylesheet
  const useStyles = makeStyles(() => ({
    root: {
      display: 'flex',
      justifyContent: 'space-between',
      flexWrap: 'wrap',
    },
  }));

  // Apply style
  const classes = useStyles();

  // State
  const [badges, setBadges] = useState([]);

  // getBadges
  const getBadges = async () => {
    if (userID !== '') {
      const userBadges = await getUserBadges(userID);
      setBadges(userBadges);
    }
  };

  // UseEffects
  useEffect(getBadges, []);

  return (
    <div className={classes.root}>
      {/* Details of user */}
      <UserCard
        profilePic={user ? `images/profile/${user.img}` : ''}
        username={user?.username}
        bio={user?.bio}
        goEditProfile={goEditProfile}
      />

      {/* List of user's badges */}
      <BadgesList
        badges={badges}
      />

      {/* Buttons to change view */}
      <HistoryCard
        goHistory={goHistory}
        goMyPosts={goMyPosts}
      />

      {/* Buttons to change view */}
      <FollowedTagsCard
        goHistory={goHistory}
        goMyPosts={goMyPosts}
      />

      {/* Buttons to leave app */}
      <LeaveCard />
    </div>
  );
};

// Types checking
Profile.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string,
    bio: PropTypes.string,
    img: PropTypes.string,
  }).isRequired,

  userID: PropTypes.number.isRequired,

  goHistory: PropTypes.func.isRequired,

  goMyPosts: PropTypes.func.isRequired,

  goEditProfile: PropTypes.func.isRequired,
};

export default Profile;
