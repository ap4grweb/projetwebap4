// React imports
import React from 'react';
import PropTypes from 'prop-types';

// Material UI
import {
  makeStyles, Avatar, Card, CardHeader, CardContent, Container,
} from '@material-ui/core/';
import AvatarGroup from '@material-ui/lab/AvatarGroup';

// Component BadgeList
const BadgesList = ({ badges }) => {
  // Stylesheet
  const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 500,
      margin: '20px',
    },
    card: {
      height: 250,
    },
    avatar: {
      margin: 'auto',
      width: theme.spacing(10),
      height: theme.spacing(10),
    },
  }));

  // Use stylesheet
  const classes = useStyles();

  return (
    <Container className={classes.root}>
      <Card className={classes.card}>
        <CardHeader
          title="Badges"
        />
        <CardContent>
          <AvatarGroup max={4} spacing="medium">
            {badges.map((badge) => (
              <Avatar
                className={classes.avatar}
                key={badge.ID}
                alt={badge.name}
                src={badge.img}
              />
            ))}
          </AvatarGroup>
        </CardContent>
      </Card>
    </Container>
  );
};

// Types Checking
BadgesList.propTypes = {
  badges: PropTypes.arrayOf(PropTypes.shape({
    ID: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
  })).isRequired,
};

export default BadgesList;
