// React imports
import React, { useState } from 'react';

// Material UI
import {
  makeStyles,
  Card, Button, Container, CardHeader, Dialog, DialogTitle, DialogContent,
  DialogContentText, DialogActions,
} from '@material-ui/core';

// Libs
import { useHistory } from 'react-router-dom';

// Local
import { logout, removeAccount, getUserId } from '../../../api';

const LeaveCard = () => {
  // Style
  const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 500,
      margin: '20px',
    },
    card: {
      height: 250,
    },
    cardAction: {
      height: '70%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      alignItems: 'center',
    },
    button: {
      width: '70%',
      height: '50px',
      background: theme.palette.secondary.main,
      '&:hover': {
        background: theme.palette.secondary.light,
      },
    },
  }));

  // Apply style
  const classes = useStyles();

  // Use history
  const history = useHistory();

  // Modal state
  const [open, setOpen] = useState(false);

  // Functions
  const handleLogout = () => {
    logout()
      .then(() => history.push('/login'));
  };

  const handleDelete = () => {
    getUserId()
      .then(removeAccount)
      .then(() => history.push('/register'))
      .catch(console.error);
  };

  return (
    <Container className={classes.root}>
      <Card className={classes.card}>
        <CardHeader
          title="Leave us"
        />
        <div className={classes.cardAction}>
          {/* Logout button */}
          <Button
            className={classes.button}
            size="small"
            color="secondary"
            variant="contained"
            onClick={handleLogout}
          >
            Just logout
          </Button>

          {/* Delete account button */}
          <Button
            className={classes.button}
            size="small"
            color="secondary"
            variant="contained"
            onClick={() => setOpen(true)}
          >
            Delete account
          </Button>

          {/* Dialog to comfirm delete */}
          <Dialog
            open={open}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">Are you sure ?</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Do you really want to delete your account ?
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => setOpen(false)} color="primary">
                Cancel
              </Button>
              <Button onClick={handleDelete} color="primary" autoFocus>
                Delete
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      </Card>
    </Container>
  );
};

export default LeaveCard;
