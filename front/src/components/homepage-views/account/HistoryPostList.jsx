// React imports
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

// Material UI
import {
  makeStyles, Button, Container, List, ListItem, Divider, ListItemText, ListItemAvatar, Avatar,
  Typography,
} from '@material-ui/core';

// Local
import {
  getMyPosts, getMyHistory, getTemplate, getPostScore, removePost,
} from '../../../api';

const HistoryPostList = ({ isReaction }) => {
  // Stylesheet
  const useStyles = makeStyles((theme) => ({
    container: {
      maxWidth: '800px',
    },
    listItem: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    deleteBtn: {
      background: '#E43D40',
      color: 'white',
      marginRight: '10px',
    },
    templateAvatar: {
      width: theme.spacing(7),
      height: theme.spacing(7),
    },
  }));

  // Use stylesheet
  const classes = useStyles();

  // States
  const [posts, setPosts] = useState([]);

  // Functions
  const fetchPosts = async () => {
    let postsList;
    if (isReaction) {
      postsList = await getMyHistory();
    } else {
      postsList = await getMyPosts();
    }
    postsList = await Promise.all(postsList.map(async (post) => {
      const { img } = await getTemplate(post.template);
      const date = new Date(post.creationTime).toLocaleDateString();
      const { score } = await getPostScore(post.ID);
      return {
        ...post, imgUrl: img, date, score,
      };
    }));
    setPosts(postsList);
  };

  // Use effects
  useEffect(fetchPosts, []);

  return (
    <Container className={classes.container}>
      <List className={classes.list}>
        {posts.map((post) => (
          <div key={post.ID}>
            <ListItem className={classes.listItem} alignItems="flex-start">
              {!isReaction
                && (
                <Button
                  variant="contained"
                  className={classes.deleteBtn}
                  onClick={() => removePost(post.ID).then(fetchPosts)}
                >
                  Delete
                </Button>
                )}
              <ListItemText
                primary={post.caption}
                secondary={(
                  <>
                    <Typography
                      component="span"
                      variant="body2"
                      className={classes.inline}
                      color="textPrimary"
                    >
                      {'Date - '}
                    </Typography>

                    {post.date}
                  </>
        )}
              />
              <ListItemText
                primary="Score"
                secondary={(
                  <>
                    <Typography
                      component="span"
                      variant="body2"
                      className={classes.inline}
                      color="textPrimary"
                    >
                      {post.score}
                    </Typography>
                  </>
        )}
              />
              {isReaction
                && (
                <ListItemText
                  primary="Reaction"
                  secondary={(
                    <>
                      <Typography
                        component="span"
                        variant="body2"
                        className={classes.inline}
                        color="textPrimary"
                      >
                        {post.reaction}
                      </Typography>
                    </>
        )}
                />
                )}
              <ListItemAvatar>
                <Avatar className={classes.templateAvatar} alt="template" src={post.imgUrl} />
              </ListItemAvatar>
            </ListItem>
            <Divider variant="inset" component="li" />
          </div>
        ))}
      </List>
    </Container>
  );
};

// Types checking
HistoryPostList.propTypes = {
  isReaction: PropTypes.bool,
};

// Default props values
HistoryPostList.defaultProps = {
  isReaction: false,
};

export default HistoryPostList;
