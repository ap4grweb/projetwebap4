// React imports
import React from 'react';
import PropTypes from 'prop-types';

// Material UI
import { Fab, makeStyles } from '@material-ui/core';

// Component GeneratedWords
const GeneratedWords = ({ wordList, setSentence, sentence }) => {
  // Stylesheet
  const useStyles = makeStyles((theme) => ({
    word: {
      [theme.breakpoints.down('sm')]: {
        padding: '3px',
        fontSize: '0.6rem',
      },
      fontSize: '0.8rem',
      fontWeight: 'bold',
      marginRight: theme.spacing(1),
      boxShadow: 'none',
      '&.MuiFab-secondary': {
        background: theme.palette.warning.main,
        '&:hover': {
          background: theme.palette.warning.light,
        },
      },
      '&.MuiFab-primary': {
        background: theme.palette.primary.main,
        '&:hover': {
          background: theme.palette.primary.light,
        },
      },
    },
  }));

  // Apply style
  const classes = useStyles();

  // Handle word click
  const handleWordClick = (wordToRemove) => {
    if (sentence.find((word) => word.value === wordToRemove)) {
      setSentence((prev) => prev.filter((word) => word.value !== wordToRemove));
    } else {
      setSentence((prev) => [...prev, { label: wordToRemove, value: wordToRemove }]);
    }
  };

  return (
    <>
      {wordList.map((word) => (
        <Fab
          className={classes.word}
          key={word.value}
          color={word.isUsed ? 'secondary' : 'primary'}
          onClick={() => handleWordClick(word.value)}
          aria-label="word"
          variant="extended"
          disabled={false}
        >
          {word.value}
        </Fab>
      ))}
    </>
  );
};

// Types Checking
GeneratedWords.propTypes = {
  wordList: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
    isUsed: PropTypes.bool,
  })).isRequired,

  sentence: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
    isUsed: PropTypes.bool,
  })).isRequired,

  setSentence: PropTypes.func.isRequired,
};

export default GeneratedWords;
