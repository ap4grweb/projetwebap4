// React imports
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import CreatableSelect from 'react-select/creatable';

// Material UI
import { createMuiTheme, ThemeProvider } from '@material-ui/core';

// Local
import customPalette from '../../../../utils/customPalette';

// Component CaptionInput
const CaptionInput = ({
  wordList, sentence, setSentence, maxWords, maxGeneratedWords, updateSentence,
}) => {
  // Palette handling
  const palette = createMuiTheme(customPalette);

  // Handle new word
  const handleSentenceChange = (newSentence) => {
    // eslint-disable-next-line no-underscore-dangle
    const nbCreatedWords = newSentence.filter((word) => word?.__isNew__).length;
    if (nbCreatedWords + maxGeneratedWords <= maxWords) {
      setSentence(newSentence);
    }
  };

  // Handler for noOptionsAvailable event
  // This function sets the message to show to the user
  const handleNoOptionsAvailable = () => {
    if (sentence.length < maxWords) {
      if (sentence.length === 0) {
        return `You can add up to ${maxWords - sentence.length} maxWords to your caption`;
      } if (maxWords - sentence.length === 1) {
        return `You can still add ${maxWords - sentence.length} more word`;
      }
      return `You can add ${maxWords - sentence.length} more words`;
    }
    return "You've reached the maximum amount of words";
  };

  // Use effects
  useEffect(updateSentence, [sentence]);

  return (
    <>
      <ThemeProvider theme={palette}>
        <CreatableSelect
          closeMenuOnSelect={false}
          noOptionsMessage={handleNoOptionsAvailable}
          isMulti
          styles={{
            multiValueLabel: (base, { data }) => ({
              ...base,
              backgroundColor: customPalette.palette.primary.main,
              // eslint-disable-next-line no-underscore-dangle
              textDecoration: ((data.__isNew__) ? 'none' : `underline ${customPalette.palette.info.main}`),
              color: 'white',
            }),
            dropdownIndicator: (base) => ({
              ...base,
              display: 'none',
            }),
            indicatorSeparator: () => ({ display: 'none' }),
          }}
          onKeyDown={(e) => { if (e.key === 32) e.preventDefault(); }}
          onChange={handleSentenceChange}
          placeholder="Write your caption here"
          value={sentence}
          options={wordList.map(({ value }) => ({ value, label: value }))}
          isValidNewOption={(e) => (sentence.length < maxWords && e.length > 0)}
        />
      </ThemeProvider>
    </>
  );
};

// Types checking
CaptionInput.propTypes = {
  wordList: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
    isUsed: PropTypes.bool,
  })).isRequired,

  sentence: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
    __isNew__: PropTypes.bool,
  })).isRequired,

  updateSentence: PropTypes.func.isRequired,

  setSentence: PropTypes.func.isRequired,

  maxWords: PropTypes.number.isRequired,

  maxGeneratedWords: PropTypes.number.isRequired,
};

export default CaptionInput;
