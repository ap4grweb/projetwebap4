// React imports
import React, { useState } from 'react';
import PropTypes from 'prop-types';

// Material UI
import {
  CssBaseline, Button, Card, Container, makeStyles, Typography, Checkbox, FormControlLabel,
} from '@material-ui/core';
import CardContent from '@material-ui/core/CardContent';

// Local
import { createNewPost } from '../../../../api';
import GeneratedWords from './GeneratedWords';
import ButtonRefreshWordList from './ButtonRefreshWordList';
import CaptionInput from './CaptionInput';
import TagList from './TagList';

// Component CreatePost
const CreatePost = ({ template, goTemplateChoose }) => {
  // Stylesheet
  const useStyles = makeStyles((theme) => ({
    root: {
      padding: theme.spacing(1),
      display: 'flex',
      width: '100%',
      [theme.breakpoints.down('lg')]: {
        display: 'block',
        width: '80%',
        margin: 'auto',
      },
      [theme.breakpoints.down('md')]: {
        display: 'block',
        width: '100%',
        margin: 'auto',
      },
    },
    details: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    content: {
      width: '100%',
      flex: '1 0 auto',
    },
    template: {
      border: '1px solid #bdbdbd',
      borderRadius: '5px',
      height: '600px',
      width: '600px',
      objectFit: 'contain',
      [theme.breakpoints.down('lg')]: {
        width: '100%',
        height: '400px',
      },
    },
    generatedWords: {
      height: '100px',
      backgroundColor: 'rgba(0,0,0,0.05)',
      textAlign: 'center',
    },
    buttonRefreshWordList: {
      float: 'left',
      width: '100px',
      height: '100%',
      zIndex: '99',
      backgroungColor: '#000',
      display: 'inline-block',
    },
    wordLabel: {
      textAlign: 'left',
      padding: theme.spacing(1),
      marginLeft: '110px',
    },
    container: {
      display: 'block',
      marginTop: '20px',
    },
    submitButton: {
      width: '300px',
      float: 'right',
      display: 'block',
      marginTop: '20px',
      color: 'white',
      background: theme.palette.primary.main,
      '&:hover': {
        background: theme.palette.primary.light,
      },
    },

  }));

  // Apply style
  const classes = useStyles();

  // States
  const [tagList, setTagList] = useState([]);
  const [sentence, setSentence] = useState([]);
  const [isPublic, setIsPublic] = useState(true);
  const [generatedWordList, setGeneratedWordList] = useState([]);
  const maxGeneratedWords = 3;
  const maxCaptionWords = 7;

  // Check if words are used
  const updateSentence = () => {
    setGeneratedWordList((prev) => {
      const newList = prev.map((word) => {
        const newWord = word;
        newWord.isUsed = sentence.find((elt) => elt.value === word.value) !== undefined;
        return newWord;
      });
      return newList;
    });
  };

  // Handle checkbox
  const handleCheckBox = (e) => {
    setIsPublic(e.target.checked);
  };

  // Handle submit
  const handleSubmit = () => {
    if (generatedWordList.filter((word) => word.isUsed).length === maxGeneratedWords) {
      // Prepare datas
      const body = {
        template: template.ID,
        caption: sentence
          .map((word) => word.value)
          .join(' '),
        tags: tagList
          .map((tag) => tag.value),
        publicVisibility: isPublic,
      };
      // Create post
      createNewPost(body)
        .then(() => goTemplateChoose())
        .catch(console.error);
    }
  };

  return (
    <Container>

      <CssBaseline />

      <Card className={classes.root}>
        <img
          className={classes.template}
          alt={template.name}
          src={template.img}
        />
        <div className={classes.details}>
          <CardContent className={classes.content}>

            <Card className={classes.generatedWords}>
              <div className={classes.buttonRefreshWordList}>
                <ButtonRefreshWordList
                  setSentence={setSentence}
                  setGeneratedWordList={setGeneratedWordList}
                  maxWords={maxGeneratedWords}
                />
              </div>
              <div className={classes.wordLabel}>
                <Typography variant="h6">Here are your words</Typography>
              </div>
              <GeneratedWords
                wordList={generatedWordList}
                sentence={sentence}
                setSentence={setSentence}
              />
            </Card>

            <div className={classes.container}>
              <Typography variant="h6">Caption</Typography>
              <CaptionInput
                className={classes.caption}
                wordList={generatedWordList}
                sentence={sentence}
                updateSentence={updateSentence}
                setSentence={setSentence}
                maxWords={maxCaptionWords}
                maxGeneratedWords={maxGeneratedWords}
              />
            </div>

            <div className={classes.container}>
              <Typography variant="h6">Tags</Typography>
              <TagList
                tagList={tagList}
                setTagList={setTagList}
                maxTags={4}
              />
            </div>

            <FormControlLabel
              className={classes.container}
              control={<Checkbox value="is-public" onChange={handleCheckBox} />}
              label="This post can be viewed without an account"
            />

            <Button variant="contained" onClick={handleSubmit} className={classes.submitButton}>
              Upload
            </Button>
          </CardContent>
        </div>

      </Card>

    </Container>
  );
};

// Types checking
CreatePost.propTypes = {
  template: PropTypes.shape({
    ID: PropTypes.number.isRequired,
    img: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,

  goTemplateChoose: PropTypes.func.isRequired,
};

export default CreatePost;
