// React imports
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

// Material UI
import { Button, makeStyles } from '@material-ui/core';
import AutorenewIcon from '@material-ui/icons/Autorenew';

// Local
import { randomWord } from '../../../../utils/wordGenerator';

// Component ButtonRefreshWordList
const ButtonRefreshWordList = ({ setSentence, setGeneratedWordList, maxWords }) => {
  // Stylesheet
  const useStyles = makeStyles((theme) => ({
    btn: {
      height: '100%',
      width: '100%',
      background: theme.palette.primary.main,
      '&:hover': {
        background: theme.palette.primary.light,
      },
    },
  }));

  // Apply style
  const classes = useStyles();

  // Generates a word list
  const createWordList = () => {
    const wordList = [];
    let word;
    for (let k = 0; k < maxWords; k += 1) {
      word = randomWord(wordList.map(({ value }) => value));
      wordList.push(
        {
          value: word,
          label: word,
          isUsed: false,
        },
      );
    }
    setSentence([]);
    setGeneratedWordList(wordList);
  };

  // UseEffects
  useEffect(createWordList, []);

  return (
    <Button className={classes.btn} variant="contained" color="secondary" onClick={() => createWordList()}>
      <AutorenewIcon />
    </Button>
  );
};

// Types Checking
ButtonRefreshWordList.propTypes = {
  setSentence: PropTypes.func.isRequired,

  setGeneratedWordList: PropTypes.func.isRequired,

  maxWords: PropTypes.number.isRequired,
};

export default ButtonRefreshWordList;
