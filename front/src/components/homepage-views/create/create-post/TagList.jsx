// React imports
import React from 'react';
import PropTypes from 'prop-types';
import CreatableSelect from 'react-select/creatable';

// Material UI
import { createMuiTheme, ThemeProvider } from '@material-ui/core';

// Local
import customPalette from '../../../../utils/customPalette';

// Component TagList
const TagList = ({ tagList, setTagList, maxTags }) => {
  // Palette handling
  const palette = createMuiTheme(customPalette);

  // Handle tagList change
  const handleTagsChange = (newTagList) => {
    if (newTagList.length <= maxTags) {
      setTagList(newTagList);
    }
  };

  // Handler for noOptionsAvailable event
  // This function sets the message to show to the user
  const handleNoOptionsAvailable = () => {
    if (tagList.length < maxTags) {
      if (tagList.length === 0) {
        return `You can add up to ${maxTags - tagList.length} tags`;
      } if (maxTags - tagList.length === 1) {
        return `You can still add ${maxTags - tagList.length} more tag`;
      }
      return `You can add ${maxTags - tagList.length} more tags`;
    }
    return "You've reached the maximum amount of tags";
  };

  return (
    <>
      <ThemeProvider theme={palette}>
        <CreatableSelect
          closeMenuOnSelect={false}
          noOptionsMessage={handleNoOptionsAvailable}
          isMulti
          onKeyDown={(e) => { if (e.key === 32) e.preventDefault(); }}
          onChange={handleTagsChange}
          placeholder="Insert some cool tags here"
          styles={{
            multiValueLabel: (base) => ({
              ...base,
              backgroundColor: customPalette.palette.primary.main,
              color: 'white',
            }),
            dropdownIndicator: (base) => ({
              ...base,
              display: 'none',
            }),
            indicatorSeparator: () => ({ display: 'none' }),
          }}
          value={tagList}
          isValidNewOption={(e) => (tagList.length < maxTags && e.length > 0)}
          formatCreateLabel={(e) => (`Add tag named: ${e}`)}
        />
      </ThemeProvider>
    </>
  );
};

// Types checking
TagList.propTypes = {
  tagList: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
    isUsed: PropTypes.bool,
  })).isRequired,

  setTagList: PropTypes.func.isRequired,

  maxTags: PropTypes.number.isRequired,
};

export default TagList;
