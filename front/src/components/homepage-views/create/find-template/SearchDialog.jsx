// React imports
import React, { useState } from 'react';
import PropTypes from 'prop-types';

// Material UI
import {
  makeStyles,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  InputLabel,
  Input,
  TextField,
  FormControl,
  Select,
} from '@material-ui/core/';
import { Search } from '@material-ui/icons';

// Component ParamsDialog
const ParamsDialog = ({ onSubmit, params }) => {
  // Stylesheet
  const useStyles = makeStyles((theme) => ({
    paramsDialog: {
      display: 'flex',
      justifyContent: 'space-around',
    },
    container: {
      display: 'flex',
      flexWrap: 'no-wrap',
      alignItems: 'flex-end',
    },
    dialog: {
      paddingRight: '8px',
      paddingBottom: '8px',
    },
    dialogTitle: {
      paddingBottom: 0,
    },

    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    button: {
      color: 'white',
      background: theme.palette.primary.main,
      '&:hover': {
        background: theme.palette.primary.light,
      },
    },
  }));

  // Apply style
  const classes = useStyles();

  // States
  const [open, setOpen] = useState(false);
  const [keyword, setKeyword] = useState('');
  const [limit, setLimit] = useState('');

  // Functions
  const handleChangeKeyword = (e) => {
    e.preventDefault();
    setKeyword(e.target.value);
  };

  const handleLimitChange = (event) => {
    setLimit(Number(event.target.value) || 10);
  };

  const handleClickOpen = () => {
    setKeyword(params.keyword);
    setLimit(params.limit);
    setOpen(true);
  };

  const handleAbort = () => {
    setOpen(false);
  };

  const handleValidate = (e) => {
    e?.preventDefault();
    onSubmit({ keyword, limit });
    setOpen(false);
  };

  return (
    <div className={classes.paramsDialog}>
      <Button onClick={handleClickOpen} variant="contained" className={classes.button}>Find a template</Button>
      <Dialog disableBackdropClick disableEscapeKeyDown open={open} onClose={handleAbort}>
        <DialogTitle className={classes.dialogTitle}>Adjust Query</DialogTitle>
        <DialogContent className={classes.dialog}>
          <form className={classes.container} onSubmit={handleValidate}>
            <FormControl>
              {' '}
              {/* Keyword */}
              <TextField
                variant="outlined"
                margin="normal"
                id="keyword"
                type="text"
                label="keyword"
                value={keyword}
                onChange={handleChangeKeyword}
                name="keyword"
                data-message-required="Please enter one keyword"
              />

            </FormControl>
            <FormControl className={classes.formControl}>
              {/* Max results input */}
              <InputLabel htmlFor="limit">Max results</InputLabel>
              <Select
                native
                value={limit}
                onChange={handleLimitChange}
                input={<Input id="limit" />}
              >
                {/* Limit to 20 templates max */}
                {/* cf: https://www.techiedelight.com/initialize-array-with-range-from-0-to-n-javascript/ */}
                {[...Array(21).keys()].map((nb) => <option key={nb} value={nb}>{nb}</option>)}
              </Select>

            </FormControl>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleAbort} variant="contained" color="secondary">
            Cancel
          </Button>
          <Button onClick={handleValidate} variant="contained" color="primary">
            <Search />
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

// Types checking
ParamsDialog.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  params: PropTypes.shape({
    limit: PropTypes.number.isRequired,
    keyword: PropTypes.string,
  }).isRequired,

};

export default ParamsDialog;
