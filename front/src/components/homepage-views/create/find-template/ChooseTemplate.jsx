// React imports
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

// Material UI
import { makeStyles, Card } from '@material-ui/core';
import AddBoxIcon from '@material-ui/icons/AddBox';

// Local
import SearchDialog from './SearchDialog';
import { getTemplates } from '../../../../api';

// Component ChooseTemplate
const ChooseTemplate = ({ goTemplateCreation, goPostCreation }) => {
  // Stylesheet
  const useStyles = makeStyles(() => ({
    card: {
      margin: '10px',
      width: '140px',
      '&:hover': {
        opacity: '0.6',
      },
    },
    addIcon: {
      margin: '10px',
      width: '140px',
      height: 'inherit',
      '&:hover': {
        color: 'grey',
      },
    },
    templates: {
      display: 'flex',
      justifyContent: 'center',
      flexWrap: 'wrap',
    },
    media: {
      width: '100%',
      height: '100%',
      objectFit: 'fit',
    },
  }));
  const classes = useStyles();

  // States
  const [templates, setTemplates] = useState([]);

  // Params for the query to fetch templates
  // params.keyword --> filter on the template name
  // params.limit --> max templates number returned by the server
  const [params, setParams] = useState({ keyword: '', limit: 20 });

  // Fetch templates list with params
  const fetchTemplates = async () => {
    const newTemplates = await getTemplates(params);
    setTemplates(newTemplates);
  };

  // UseEffects
  useEffect(fetchTemplates, [params]);

  return (
    <div className={classes.chooseTemplate}>
      <SearchDialog
        className={classes.dialog}
        params={params}
        onSubmit={setParams}
      />
      {/* Create new template icon */}
      <div className={classes.templates}>
        <AddBoxIcon
          className={classes.addIcon}
          title="Create template"
          onClick={goTemplateCreation}
        />
        {/* Template list */}
        {templates.map((template) => (
          <Card
            key={template.ID}
            className={classes.card}
            onClick={() => goPostCreation(template)}
          >
            <img
              className={classes.media}
              alt={template.name}
              src={template.img}
            />
          </Card>
        ))}
      </div>
    </div>
  );
};

// Types checking
ChooseTemplate.propTypes = {
  goTemplateCreation: PropTypes.func.isRequired,
  goPostCreation: PropTypes.func.isRequired,
};

export default ChooseTemplate;
