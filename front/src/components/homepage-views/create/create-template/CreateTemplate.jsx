// React imports
import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

// Material UI
import {
  makeStyles, Button, TextField, Card, InputLabel, CardHeader, Container,
} from '@material-ui/core';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import PublishIcon from '@material-ui/icons/Publish';

// Exif
import EXIF from 'exif-js';

// Validator
import { isAlphanumeric } from 'validator';

// Local
import { postTemplate, getTemplate } from '../../../../api';

// Component CreateTemplate
const CreateTemplate = ({ goPostCreation }) => {
  // Stylesheet
  const useStyles = makeStyles(() => ({
    container: {
      margin: 'auto',
      maxWidth: '600px',
    },
    card: {
      height: '300px',
      marginBottom: '20px',
      '&:hover': {
        opacity: '0.6',
      },
    },
    exifsWarning: {
      marginBottom: '20px',
      fontWeight: 'bold',
      display: 'flex',
      alignItems: 'center',
      padding: '3px',
    },
    exifsText: {
      fontFamily: 'monospace',
      height: '80px',
      background: '#1a1a1a',
      color: 'lime',
      overflow: 'scroll',
      padding: '20px',
      paddingTop: '10px',
    },
    exifsCard: {
      marginBottom: '10px',
    },
    media: {
      width: '100%',
      height: '100%',
      objectFit: 'fit',
    },
  }));

  // Apply style
  const classes = useStyles();

  // States
  const [name, setName] = useState('');
  const [img, setImg] = useState('');
  const [metaDatas, setMetaDatas] = useState();
  const [error, setError] = useState('');

  // Ref
  const fileInputRef = useRef();

  // Functions
  const handleChangeName = (e) => {
    e.preventDefault();
    setName(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (name.length <= 0) {
      setError('Please enter a template name...');
    } else if (!fileInputRef.current.files[0]) {
      setError('Please select a file...');
    } else if (error === '') {
      const formData = new FormData();
      const imageFile = fileInputRef.current.files[0];
      formData.append('file', imageFile);
      formData.append('name', name);

      postTemplate(formData)
        .then(getTemplate)
        .then(goPostCreation)
        .catch(console.error);
    }
  };

  const handleFileChange = () => {
    const imageFile = fileInputRef.current.files[0];
    // Reading exifs
    const reader = new FileReader();
    reader.onload = function (e) {
      const image = document.createElement('img');
      image.src = e.target.result;
      setImg(image);
    };
    reader.readAsDataURL(imageFile);
  };

  const getExifs = () => {
    EXIF.getData(img, function () {
      const allMetaData = EXIF.getAllTags(this);
      setMetaDatas(allMetaData);
    });
  };

  // Use Effect
  useEffect(getExifs, [img]);
  useEffect(() => {
    if ((!isAlphanumeric(name) || name.length < 3) && name.length > 0) {
      setError('Please enter a valid template name...');
    } else {
      setError('');
    }
  }, [name]);

  return (
    <Container className={classes.container}>

      <form className={classes.form} onSubmit={handleSubmit} noValidate>
        {/* Name for the template  */}
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="name"
          type="text"
          label="Template name"
          value={name}
          onChange={handleChangeName}
          name="name"
        />

        {/* Upload button */}
        <InputLabel htmlFor="browse-input">
          <Card
            className={classes.card}
          >
            {
              img?.src
                ? (
                  <img
                    className={classes.media}
                    alt={name}
                    src={img.src}
                  />
                )
                : <PublishIcon className={classes.media} />
            }

          </Card>

        </InputLabel>
        <input
          ref={fileInputRef}
          style={{ display: 'none' }}
          accept="image/*"
          type="file"
          onChange={handleFileChange}
          id="browse-input"
        />

        {/* Exifs */}
        <Card className={classes.exifsCard} variant="outlined">
          <CardHeader
            title="EXIF DATA"
          />

          <div className={classes.exifsText} color="textSecondary">
            {metaDatas
              ? Object.keys(metaDatas).map((key) => (
                <div>
                  {`${key}  :  ${JSON.stringify(metaDatas[key])}`}
                </div>
              ))
              : 'No EXIF data...'}
          </div>
        </Card>

        {/* Message about exif removing */}
        <Card className={classes.exifsWarning}>
          <ErrorOutlineIcon />
          Exif data will be removed for your privacy
        </Card>

        {error !== '' && error }

        <Button
          type="submit"
          variant="contained"
          color="primary"
          fullWidth
        >
          Submit
        </Button>

      </form>
    </Container>
  );
};

// Types Checking
CreateTemplate.propTypes = {
  goPostCreation: PropTypes.func.isRequired,
};

export default CreateTemplate;
