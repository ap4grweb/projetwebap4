// React imports
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

// Material UI
import {
  createMuiTheme, makeStyles, ThemeProvider, CssBaseline, BottomNavigation, BottomNavigationAction,
  Drawer, MenuItem, MenuList, Container, Typography,
} from '@material-ui/core';
import {
  Explore, Favorite, AccountCircle, Create,
} from '@material-ui/icons';

// Local
import { getUserId } from '../../api';
import FeedView from '../homepage-views/FeedView';
import ExploreView from '../homepage-views/ExploreView';
import CreateView from '../homepage-views/CreateView';
import AccountView from '../homepage-views/AccountView';
import customPalette from '../../utils/customPalette';

// Component Homepage
const HomePage = () => {
  // Settings for GUI
  const drawerWidth = 200;
  const bottomNavHeight = 75;

  // States
  const [pageIndex, setPageIndex] = useState(0);
  const [windowHeight, setwindowHeight] = useState(window.innerHeight - 1); // 1px offset
  const [windowWidth, setwindowWidth] = useState(window.innerWidth);
  const viewNames = ['My Feed', 'Explore', 'Create', 'My Account'];
  const [isLoading, setIsLoading] = useState(true);

  // Stylesheet
  const useStyles = makeStyles((theme) => ({
    drawer: {
      flexShrink: 0,
      padding: theme.spacing(0),
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      },
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth / 3,
      },
      [theme.breakpoints.up('lg')]: {
        width: drawerWidth,
      },
    },
    drawerPaper: {
      flexShrink: 0,
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth / 3,
      },
      [theme.breakpoints.up('lg')]: {
        width: drawerWidth,
      },
    },
    bottomNav: {
      zIndex: 1,
      width: windowWidth,
      position: 'fixed',
      height: bottomNavHeight,
      bottom: 0,
      left: 0,
      flexShrink: 0,
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
    spacer: {
      height: bottomNavHeight + 10,
    },
    toolbar: {
      width: '100%',
      padding: theme.spacing(0),
    },
    menuText: {
      marginLeft: '25px',
      fontSize: 20,
      [theme.breakpoints.down('md')]: {
        display: 'none',
      },
    },
    drawerItem: {
      height: windowHeight / 6,
      margin: '0px',
      transition: 'all 0.125s ease-out',
      '&.Mui-selected': {
        transition: 'all 0.125s ease-out',
        height: windowHeight / 2,
        color: customPalette.palette.primary.main,
        boxShadow: `10px 0px 0px 0px ${customPalette.palette.primary.main} inset`,
        '&:hover': {
          boxShadow: `10px 0px 0px 0px ${customPalette.palette.primary.light}  inset`,
        },
      },
      '&:hover': {
        boxShadow: `10px 0px 0px 0px ${customPalette.palette.primary.light} inset`,
      },
    },
    menuList: {
      padding: theme.spacing(0),
    },
    root: {
      display: 'flex',
    },
    contentContainer: {
      overflowX: 'hidden',
      width: windowWidth - drawerWidth,
      height: windowHeight,
      flexGrow: 1,
      padding: theme.spacing(1),
      [theme.breakpoints.down('sm')]: {
        height: windowHeight - bottomNavHeight,
        width: windowWidth,
      },
    },
    bottomNavElm: {
      '&.Mui-selected': { color: '#1976d2' },
    },
  }));

  // Apply style
  const classes = useStyles();

  // Palette handling
  const palette = createMuiTheme(customPalette);

  // Handler for window size
  function updateWindowSize() {
    setwindowHeight(window.innerHeight - 1); // 1px offset
    setwindowWidth(window.innerWidth);
  }

  // Listen for change of window size
  window.addEventListener('resize', updateWindowSize);

  // Use History
  const history = useHistory();

  // Navigation bars handler
  const setNav = (e, index) => {
    e.preventDefault();
    setPageIndex(index);
  };

  // Use effect
  useEffect(() => {
    getUserId()
      .then((id) => {
        if (id !== '') {
          setIsLoading(false);
        } else {
          history.push('/login');
        }
      })
      .catch(console.error);
  }, []);

  return (
    <div className={classes.root}>
      {isLoading
        ? (
          <div className={classes.loading}>
            Loading...
          </div>
        )
        : (
          <>
            <CssBaseline />
            <ThemeProvider theme={palette}>
              <Drawer
                className={classes.drawer}
                classes={{ paper: classes.drawerPaper }}
                variant="permanent"
                anchor="left"
              >
                <MenuList className={classes.menuList}>
                  <MenuItem
                    key="My feed"
                    selected={pageIndex === 0}
                    className={classes.drawerItem}
                    onClick={(e) => setNav(e, 0)}
                  >
                    <Favorite />
                    <Typography className={classes.menuText}>{viewNames[0]}</Typography>
                  </MenuItem>
                  <MenuItem
                    key="Explore"
                    selected={pageIndex === 1}
                    className={classes.drawerItem}
                    onClick={(e) => setNav(e, 1)}
                  >
                    <Explore />
                    <Typography className={classes.menuText}>{viewNames[1]}</Typography>
                  </MenuItem>
                  <MenuItem
                    key="Create"
                    selected={pageIndex === 2}
                    className={classes.drawerItem}
                    onClick={(e) => setNav(e, 2)}
                  >
                    <Create />
                    <Typography className={classes.menuText}>{viewNames[2]}</Typography>
                  </MenuItem>
                  <MenuItem
                    key="My account"
                    selected={pageIndex === 3}
                    className={classes.drawerItem}
                    onClick={(e) => setNav(e, 3)}
                  >
                    <AccountCircle />
                    <Typography className={classes.menuText}>{viewNames[3]}</Typography>
                  </MenuItem>
                </MenuList>
              </Drawer>
              <BottomNavigation
                value={pageIndex}
                onChange={(e, newValue) => setNav(e, newValue)}
                showLabels
                className={classes.bottomNav}
              >
                <BottomNavigationAction
                  className={classes.bottomNavElm}
                  label={viewNames[0]}
                  icon={<Favorite />}
                />
                <BottomNavigationAction
                  className={classes.bottomNavElm}
                  label={viewNames[1]}
                  icon={<Explore />}
                />
                <BottomNavigationAction
                  className={classes.bottomNavElm}
                  label={viewNames[2]}
                  icon={<Create />}
                />
                <BottomNavigationAction
                  className={classes.bottomNavElm}
                  label={viewNames[3]}
                  icon={<AccountCircle />}
                />
              </BottomNavigation>
              <Container className={classes.contentContainer} maxWidth={false}>
                <h1>
                  Caption Your Meme
                </h1>
                <hr />
                {pageIndex === 0 && <FeedView />}
                {pageIndex === 1 && <ExploreView />}
                {pageIndex === 2 && <CreateView />}
                {pageIndex === 3 && <AccountView />}
                <div className={classes.spacer} />

              </Container>
            </ThemeProvider>
          </>
        )}
    </div>
  );
};

export default HomePage;
