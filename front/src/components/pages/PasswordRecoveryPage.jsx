// React imports
import React, { useState, useEffect } from 'react';
import { isAlphanumeric, isEmail } from 'validator';

// Material UI
import {
  createMuiTheme, makeStyles, ThemeProvider, CssBaseline,
  Button, TextField, Box, Typography, Container, Link,
} from '@material-ui/core';

// Local
import theme from '../../utils/commonStyle';
import customPalette from '../../utils/customPalette';
import Footer from '../Footer';

// Component PasswordRecoveryForm
const PasswordRecoveryForm = () => {
  // Palette handling
  const palette = createMuiTheme(customPalette);

  // Theme handling
  const useStyles = makeStyles(theme);
  const classes = useStyles();

  // Inputs
  const [account, setAccount] = useState('');

  // Inputs validation
  const [accountIsValid, setAccountIsValid] = useState(false);

  // Submit management
  const [submitEnabled, setSubmitEnabled] = useState(false);

  // Controlled inputs
  function handleChangeAccount(e) {
    setAccount(e.target.value);
  }

  // Errors
  const [accountError, setAccountError] = useState('');

  const manageErrors = () => {
    // Account errors
    if (account.length > 0 && !accountIsValid) {
      if (!isAlphanumeric(account)) {
        setAccountError(
          'This is neither an email address or a valid user name',
        );
      }
    } else {
      setAccountError('');
    }
  };

  // Check function
  const checkAccount = () => {
    if (isEmail(account)) {
      setAccountIsValid(true);
    } else if (isAlphanumeric(account) && account.length > 3) {
      setAccountIsValid(true);
    } else {
      setAccountIsValid(false);
    }
  };

  const checkForm = () => {
    setSubmitEnabled(accountIsValid);
  };

  // useEffect's
  useEffect(checkAccount, [account]);
  useEffect(checkForm, [account]);
  useEffect(manageErrors, [accountIsValid]);

  function handleSubmit(e) {
    e.preventDefault();
    window.location.replace(window.atob('aHR0cHM6Ly9pLmltZ3VyLmNvbS9NeEFFOFdwLm1wNA=='));
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <ThemeProvider theme={palette}>
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            Password Recovery
          </Typography>
          <form
            className={classes.form}
            noValidate
            onSubmit={(e) => handleSubmit(e)}
          >
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="login"
              type="text"
              label="Email or Username"
              value={account}
              onChange={handleChangeAccount}
              name="login"
              data-message-required="Please enter your email address or username"
              autoComplete="login"
            />
            {accountError !== '' && (
            <div className="account-validity">{accountError}</div>
            )}
            <Button
              type="submit"
              disabled={!submitEnabled}
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Reset my password
            </Button>
            <Link href="/login" variant="body2">
              Take me back to login page !
            </Link>
          </form>
        </div>
        <Box mt={8}>
          <Footer />
        </Box>
      </ThemeProvider>
    </Container>
  );
};
export default PasswordRecoveryForm;
