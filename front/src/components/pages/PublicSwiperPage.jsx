// React imports
import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';

// Material UI
import {
  Button, Container, LinearProgress, makeStyles,
} from '@material-ui/core/';
import {
  ThumbUp, ThumbDown, Favorite, NotInterested,
} from '@material-ui/icons';

// Local
import { getPublicPost, getTemplate } from '../../api';
import Swiper from '../swiper/Swiper';
import { finalPost, finalTemplate } from '../../utils/finalPost';

// Component PublicSwiper
const PublicSwiper = () => {
  const { id } = useParams();

  const history = useHistory();
  // States
  const [post, setPost] = useState();
  const [templateImg, setTemplateImg] = useState();
  const [swiperKey, setSwiperKey] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  // Stylesheet
  const useStyles = makeStyles((theme) => ({
    progressBar: {
      display: isLoading ? 'block' : 'none',
      opacity: '0.5',
      width: '100%',
    },
    postContainer: {
      display: isLoading ? 'none' : 'block',
      margin: 'auto',
      [theme.breakpoints.up('sm')]: {
        width: '100%',
        padding: '0px',
      },
      [theme.breakpoints.up('md')]: {
        width: '80%',
        padding: '0px',
      },
      [theme.breakpoints.up('lg')]: {
        paddingTop: '20px',
        width: '60%',
      },
    },
    buttons: {
      display: 'flex',
      justifyContent: 'space-between',
      marginTop: '10px',
      maxWidth: '500px',
    },
    likeBtn: {
      background: '#2946de',
      '&:hover': {
        background: '#1d175b',
      },
    },
    dislikeBtn: {
      background: '#970609',
      '&:hover': {
        background: '#780103',
      },
    },
    superLikeBtn: {
      background: '#e7273e',
      '&:hover': {
        background: '#7e0446',
      },
    },
    superDislikeBtn: {
      background: 'black',
      '&:hover': {
        background: 'white',
        color: 'black',
      },
    },
  }));

  // Apply styles
  const classes = useStyles();

  // Functions
  const fetchPost = async () => {
    const newPost = await getPublicPost(id);
    if (newPost === '') {
      setPost(finalPost);
      setTemplateImg(finalTemplate);
    } else {
      setPost(newPost);
      const newTemplate = await getTemplate(newPost.template);
      setTemplateImg(newTemplate);
    }
    setIsLoading(false);
    setTimeout(() => setSwiperKey((prev) => prev + 1), 500);
  };

  const handleSwipe = () => {
    history.push('/register');
  };

  // Use effects
  useEffect(fetchPost, []);

  return (
    <>
      <LinearProgress className={classes.progressBar} />
      <div className={classes.postContainer}>
        <Swiper
          key={swiperKey}
          postId={post ? `public/${post.ID}` : ''}
          img={templateImg?.img}
          caption={post?.caption}
          locked
          onDislike={handleSwipe}
          onSuperLike={handleSwipe}
          onSuperDislike={handleSwipe}
          onSwipeEnd={handleSwipe}
        />
        <Container className={classes.buttons}>
          {/* SuperLike button */}
          <Button className={classes.superLikeBtn} variant="contained" color="primary">
            <Favorite />
          </Button>
          {/* Like Button */}
          <Button className={classes.likeBtn} variant="contained" color="primary">
            <ThumbUp />
          </Button>
          {/* Dislike button */}
          <Button className={classes.dislikeBtn} variant="contained" color="primary">
            <ThumbDown />
          </Button>
          {/* SuperDislike button */}
          <Button className={classes.superDislikeBtn} variant="contained" color="primary">
            <NotInterested />
          </Button>
        </Container>
      </div>
    </>
  );
};

export default PublicSwiper;
