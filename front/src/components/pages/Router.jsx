// React imports
import React from 'react';
import {
  BrowserRouter, Route, Switch, Redirect,
} from 'react-router-dom';

// Local
import PasswordRecoveryPage from './PasswordRecoveryPage';
import LoginPage from './LoginPage';
import RegisterPage from './RegisterPage';
import HomePage from './HomePage';
import SplashPage from './SplashPage';
import PublicSwiperPage from './PublicSwiperPage';

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/">
        <SplashPage />
      </Route>
      <Route exact path="/login">
        <LoginPage />
      </Route>
      <Route exact path="/register">
        <RegisterPage />
      </Route>
      <Route exact path="/recovery">
        <PasswordRecoveryPage />
      </Route>
      <Route exact path="/home">
        <HomePage />
      </Route>
      <Route path="/public/posts/:id">
        <PublicSwiperPage />
      </Route>
      <Redirect to="/home" />
    </Switch>
  </BrowserRouter>
);

export default Router;
