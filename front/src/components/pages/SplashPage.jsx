// React imports
import React from 'react';
import { useHistory } from 'react-router-dom';

// Local
import './SplashPage.css';

const Splash = () => {
  // History
  const history = useHistory();

  // Redirect the user after the set delay
  const redirectPage = () => {
    const tID = setTimeout(() => {
      history.push('/login');
      window.clearTimeout(tID);
    }, 2000);
  };

  // Call the function on page load
  redirectPage();

  return (
    <div className="container">
      <div className="content">
        <h2 className="frame">Caption Your Meme</h2>
      </div>
    </div>
  );
};

export default Splash;
