// React imports
import React, {
  useState, useCallback, useEffect,
} from 'react';
import { useHistory } from 'react-router-dom';

// Material UI
import {
  createMuiTheme, makeStyles, ThemeProvider, CssBaseline,
  Button, TextField, FormControlLabel, Checkbox, Link, Box, Typography, Container,
} from '@material-ui/core';

// Validator
import { isStrongPassword, isAlphanumeric, isEmail } from 'validator';

// Recaptcha
import ReCAPTCHA from 'react-google-recaptcha';

// Local
import theme from '../../utils/commonStyle';
import customPalette from '../../utils/customPalette';
import { register } from '../../api';
import Footer from '../Footer';

// Component RegisterForm
const RegisterForm = () => {
  // Palette handling
  const palette = createMuiTheme(customPalette);

  // Theme handling
  const useStyles = makeStyles(theme);
  const classes = useStyles();

  // History
  const history = useHistory();

  // Inputs
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');

  // Outputs
  const [welcomeMessage, setWelcomeMessage] = useState('Sign In');

  // Inputs validation
  const [usernameIsValid, setUsernameIsValid] = useState(true);
  const [emailIsValid, setEmailIsValid] = useState(true);
  const [passwordsMatch, setPasswordsMatch] = useState(true);
  const [passwordIsValid, setPasswordIsValid] = useState(true);
  const [captchaIsValid, setCaptchaIsValid] = useState(false);

  // Submit management
  const [isLoading, setLoading] = useState(false);
  const [submitEnabled, setSubmitEnabled] = useState(false);

  // Errors
  const [emailError, setEmailError] = useState('');
  const [usernameError, setUsernameError] = useState('');
  const [passwordError, setPasswordError] = useState('');

  // Functions
  const manageErrors = () => {
    // Email errors
    if (email.length > 0 && !emailIsValid) {
      setEmailError('The email address must be valid');
    } else {
      setEmailError('');
    }
    // Username errors
    if (username.length > 0 && !usernameIsValid) {
      if (!isAlphanumeric(username)) {
        setUsernameError('The username must contain alphanumeric caracters only');
      } else if (username.length < 3) {
        setUsernameError('The username is too short');
      } else if (username.length > 50) {
        setUsernameError('The username is too long');
      }
    } else {
      setUsernameError('');
    }
    // Password errors
    if (password.length > 0 && (!passwordIsValid || !passwordsMatch)) {
      if (!isStrongPassword(password)) {
        setPasswordError('Password is not strong enough');
      } else if (!passwordsMatch) {
        setPasswordError('Passwords must match');
      }
    } else {
      setPasswordError('');
    }
  };

  const checkEmail = () => {
    setEmailIsValid(isEmail(email));
  };

  const checkUsername = () => {
    setUsernameIsValid(isAlphanumeric(username) && username.length > 3 && username.length < 50);
  };

  const checkMatchingPasswords = () => {
    setPasswordsMatch(password === passwordConfirmation);
  };

  const checkPasswordStrength = () => {
    setPasswordIsValid(isStrongPassword(password));
  };

  const checkForm = () => {
    // If all inputs are valid, enable submit
    setSubmitEnabled(!isLoading
                      && emailIsValid
                      && passwordIsValid
                      && passwordsMatch
                      && captchaIsValid);
  };

  const handleChangeEmail = (e) => {
    setEmail(e.target.value);
  };

  const handleChangeUsername = (e) => {
    setUsername(e.target.value);
  };

  const handleChangePassword = (e) => {
    setPassword(e.target.value);
  };

  const handleChangePasswordConfirmation = (e) => {
    setPasswordConfirmation(e.target.value);
  };

  const handleWelcomeMessage = () => {
    if (username.length > 3) {
      setWelcomeMessage(`Welcome, ${username} !`); // Set the welcome message
    } else {
      setWelcomeMessage('Sign In'); // Set the welcome message
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (submitEnabled) {
      setLoading(true);
      const body = {
        mail: email,
        username,
        password,
      };

      const response = await register(body);
      if (response === 'success') {
        history.push('/home');
      }
    }
  };

  // Use Effect
  useEffect(checkEmail, [email]);
  useEffect(checkUsername, [username]);
  useEffect(checkPasswordStrength, [password]);
  useEffect(checkMatchingPasswords, [password, passwordConfirmation]);
  useEffect(checkForm, [email, username, password, passwordConfirmation, captchaIsValid]);
  useEffect(manageErrors, [
    usernameIsValid,
    emailIsValid,
    passwordsMatch,
    passwordIsValid,
    captchaIsValid,
    captchaIsValid,
  ]);

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <ThemeProvider theme={palette}>
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            {welcomeMessage}
          </Typography>
          <form className={classes.form} noValidate onSubmit={(e) => handleSubmit(e)}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              type="email"
              label="Email"
              value={email}
              onChange={handleChangeEmail}
              name="email"
              autoComplete="email"
            />
            { emailError !== '' && <div className="email-validity">{emailError}</div> }
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="username"
              type="text"
              label="Username"
              value={username}
              onBlur={handleWelcomeMessage}
              onChange={handleChangeUsername}
              name="username"
              autoComplete="username"
            />
            { usernameError !== '' && <div className="username-validity">{usernameError}</div> }
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              onChange={handleChangePassword}
              label="Password"
              type="password"
              id="password"
              autoComplete="password"
            />
            { passwordError !== '' && <div className="password-error">{passwordError}</div> }
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="passwordConfirmation"
              onChange={handleChangePasswordConfirmation}
              label="Password confirmation"
              type="password"
              id="passwordConfirmation"
              autoComplete="passwordConfirmation"
            />
            <ReCAPTCHA
              required
              sitekey="6LfjGsoaAAAAANN3fQaKbSgdv9g6dC_gKoQWE5E3"
              onChange={useCallback(() => setCaptchaIsValid(true))}
              onExpired={() => setCaptchaIsValid(false)}
              hl="en"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Keep me logged in !"
            />
            <Button
              type="submit"
              disabled={!submitEnabled}
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
            </Button>
            <Link href="/login" variant="body2">
              I already have an account !
            </Link>
          </form>
        </div>
        <Box mt={8}>
          <Footer />
        </Box>
      </ThemeProvider>
    </Container>
  );
};
export default RegisterForm;
