// React imports
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

// Material UI
import {
  createMuiTheme, makeStyles, ThemeProvider, CssBaseline,
  TextField, FormControlLabel, Checkbox, Link, Grid, Box, Typography, Container, Avatar, Button,
} from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';

// Validator
import { isEmail } from 'validator';

// Local
import { login, getProfilePic } from '../../api';
import theme from '../../utils/commonStyle';
import customPalette from '../../utils/customPalette';
import Footer from '../Footer';

// Component LoginForm
const LoginForm = () => {
  // Palette handling
  const palette = createMuiTheme(customPalette);

  // Theme handling
  const useStyles = makeStyles(theme);
  const classes = useStyles();

  // History
  const history = useHistory();

  // States
  const [account, setAccount] = useState('');
  const [password, setPassword] = useState('');
  const [profilePic, setProfilePic] = useState('');
  const [welcomeMessage, setWelcomeMessage] = useState('Sign in');
  const [accountIsValid, setAccountIsValid] = useState(false);
  const [passwordIsValid, setPasswordIsValid] = useState(true);
  const [isLoading, setLoading] = useState(false);
  const [submitEnabled, setSubmitEnabled] = useState(false);
  const [accountError, setAccountError] = useState('');

  // Functions
  function handleChangeAccount(e) {
    setAccount(e.target.value);
  }

  function handleChangePassword(e) {
    setPassword(e.target.value);
  }

  const manageErrors = () => {
    if (account.length === 0 && accountIsValid) {
      setAccountError('');
    }
  };

  const checkAccount = () => {
    if (isEmail(account)) {
      setAccountIsValid(true);
    } else if (account.length > 3) {
      setAccountIsValid(true);
    } else {
      setAccountIsValid(false);
    }
  };

  const checkPassword = () => {
    setPasswordIsValid(password.length > 3);
  };

  const checkForm = () => {
    setSubmitEnabled(!isLoading
                      && accountIsValid
                      && passwordIsValid);
  };

  // Use Effect
  useEffect(checkAccount, [account]);
  useEffect(checkPassword, [password]);
  useEffect(checkForm, [account, password]);
  useEffect(manageErrors, [accountIsValid, passwordIsValid]);

  async function handleSubmit(e) {
    e.preventDefault();
    if (submitEnabled) {
      setLoading(true);
      const accountAttribute = isEmail(account) ? 'mail' : 'username';
      const body = {
        [accountAttribute]: account,
        password,
      };
      const response = await login(body);
      if (response === 'success') {
        history.push('/home');
      } else {
        setAccountError('Invalid credentials, check your username and password');
      }
    }
  }

  function resetData() {
    setProfilePic(''); // Reset the profile picture
    setWelcomeMessage('Sign in'); // Reset the welcome message
  }

  async function handleAccountPic() {
    const type = isEmail(account) ? 'mail' : 'username'; // Check the type of login

    // Check if the entered account is at least 4 characters to avoid useless requests
    if (account.length > 3) {
      const picture = await getProfilePic(type, account);
      if (picture) {
        setProfilePic(picture); // Set the profile picture
        setWelcomeMessage(`Welcome back ${account} !`); // Set the welcome message
      } else {
        resetData(); // Reset profile image and welcome message
      }
    } else {
      resetData(); // Reset profile image and welcome message
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <ThemeProvider theme={palette}>
        <div className={classes.paper}>
          <Avatar src={profilePic}>
            <PersonIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            {welcomeMessage}
          </Typography>
          <form className={classes.form} noValidate onSubmit={(e) => handleSubmit(e)}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="login"
              type="text"
              label="Email or Username"
              value={account}
              onChange={handleChangeAccount}
              onBlur={handleAccountPic}
              name="login"
              data-message-required="Please enter your email address or username"
              autoComplete="login"
            />
            {accountError !== '' && <div className="account-validity">{accountError}</div>}
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              onChange={handleChangePassword}
              label="Password"
              type="password"
              id="password"
              autoComplete="new-password"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Keep me logged in !"
            />
            <Button
              type="submit"
              disabled={!submitEnabled}
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="/recovery">
                  I forgot my password
                </Link>
              </Grid>
              <Grid item>
                <Link href="/register">
                  Register
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={8}>
          <Footer />
        </Box>
      </ThemeProvider>
    </Container>
  );
};

export default LoginForm;
