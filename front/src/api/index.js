import axios from 'axios';

const baseURL = '';
const API = axios.create({ baseURL });

// Login user
export const login = (formData) => API.post('/users/login', formData)
  .then((response) => {
    if (response?.data?.user) {
      return 'success';
    }
    return response;
  });

// Register user
export const register = (formData) => API.post('/users/register', formData)
  .then((response) => {
    if (response.status === 201) {
      return 'success';
    }
    return response;
  });

// Logout user
export const logout = () => API.get('/users/logout');

// Get profile pic (for the login form)
export const getProfilePic = (type, account) => API.get(`/users/photo/${type}=${account}`)
  .then((response) => {
    if (response.status === 200) {
      return response.data.url;
    }
    return '';
  })
  // eslint-disable-next-line no-console
  .catch(console.error);

// Get user id of the authenticated user
export const getUserId = () => API.get('/jwtid')
  .then((response) => {
    if (response.status === 200 && response.data !== 'No token !') {
      return response.data.id;
    } if (response.status === 304) {
      return -1;
    }
    return '';
  })
// eslint-disable-next-line no-console
  .catch(console.error);

// Get a user by id
export const getUser = (id) => API.get(`/users/${id}`)
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  })
// eslint-disable-next-line no-console
  .catch(console.error);

// Edit profile
export const patchPofile = (id, formData) => API.patch(`/users/${id}`, formData, {
  headers: { 'Content-Type': 'multipart/form-data' },
})
  .then((response) => response);

// Remove  account
export const removeAccount = (id) => API.delete(`/users/${id}`)
  .then((response) => {
    if (response.status === 204) {
      return response.data;
    }
    return '';
  });

// Get list of badges of a user
export const getUserBadges = (id) => API.get(`/users/${id}/badges`)
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  });

// Fetch post from my feed
export const getFeed = () => API.get('/posts/feed')
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  })
  // eslint-disable-next-line no-console
  .catch(console.error);

// Fetch post by tag
export const getPostByTag = (tag) => API.get(`/posts/tag/${tag}`)
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  })
  // eslint-disable-next-line no-console
  .catch(console.error);

// Fetch my posts
export const getMyPosts = () => API.get('/posts/mine')
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  });

// Fetch my posts
export const getMyHistory = () => API.get('/posts/history')
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  })
// eslint-disable-next-line no-console
  .catch(console.error);

// Fetch  post score
export const getPostScore = (id) => API.get(`/posts/${id}/score`)
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  });

// Fetch  post author
export const getPostAuthor = (id) => API.get(`/posts/${id}/author`)
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  });

// Fetch public post by id
export const getPublicPost = (id) => API.get(`/posts/public/${id}`)
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  })
  // eslint-disable-next-line no-console
  .catch(console.error);

// Remove  post
export const removePost = (id) => API.delete(`/posts/${id}`)
  .then((response) => {
    if (response.status === 204) {
      return response.data;
    }
    return '';
  })

// eslint-disable-next-line no-console
  .catch(console.error);

// Get a templates list
export const getTemplates = (params) => API.get('/templates/', { params })
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  })
  // eslint-disable-next-line no-console
  .catch(console.error);

// Get a template url from an id
export const getTemplate = (id) => API.get(`/templates/${id}`)
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  })
  // eslint-disable-next-line no-console
  .catch(console.error);

// Put a reaction to a post
export const putReaction = (postId, reaction) => API.patch(`/posts/${postId}`, { reaction })
  .then((response) => {
    if (response.status === 200) {
      return 'success';
    }
    return response;
  });

// Post template
export const postTemplate = (formData) => API.post('/templates', formData, {
  headers: { 'Content-Type': 'multipart/form-data' },
})
  .then((response) => {
    if (response.status === 200) {
      return response.data.id;
    }
    return response;
  });

// Create new post
export const createNewPost = (formData) => API.post('/posts', formData)
  .then((response) => {
    if (response.status === 200) {
      return response.data.id;
    }
    return response;
  });

// Get tags list
export const getTags = () => API.get('/tags')
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  });

// Get my tags list
export const getMyTags = () => API.get('/posts/feed/mytags')
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  });

// Add tag to my tags list
export const addToMyTags = (formData) => API.post('/posts/feed', formData)
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  });

// Remove tag from my tags list
export const removeFromMyTags = (formData) => API.delete('/posts/feed', { data: formData })
  .then((response) => {
    if (response.status === 200) {
      return response.data;
    }
    return '';
  });
